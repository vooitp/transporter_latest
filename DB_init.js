const mongoose = require('mongoose');
// const Admin = require('./backend/models/Admin');
// const Driver = require('./backend/models/Driver');
// const Client = require('./backend/models/Client');
// const Vehicle = require('./backend/models/Vehicle');
// const TransportEvent = require('./backend/models/TransportEvent');

mongoose.Promise = global.Promise;

let url = 'mongodb+srv://vooit:qwerty123456@transporter-oah1z.mongodb.net/transporter?retryWrites=true';

/*
mongoose.connect(url, { useNewUrlParser: true })
    .then(() => {
        let dbo = db.db("transporter");
        dbo.vehicles.update({},{"photos":[]},{"multi":true});
        console.log('Connected to database after build && deploy!');
    })
    .catch((err) => {
    throw err;
    console.log('Connection failed!');
    });
*/
/*
mongoose.connect(url, function(err, db) {
    if (err) throw err;
    let dbo = db.db("transporter");
    mongoose.connection.db.collection('vehicles').update({},{"photos":[]},{"multi":true});
});
*/

var con = mongoose.connect(url);
mongoose.connection.on('open', function(){
    con.connection.db.collection('vehicles').update({},{"photos":[]},{"multi":true});
});

/*
mongoose.connect("mongodb://localhost:27017/transporter", { useNewUrlParser: true })
    .then(() => {
        mongoose.connection.db.dropDatabase().then(() => {
            console.log("Connected to database!");
            const admin = new Admin({
                email: 'admin@gmail.com',
                password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
                firstName: 'Admin',
                lastName: 'admin'
            });

            const driver = new Driver({
                _id: '5c83e9323380f56779712e7f',
                email: 'driver@gmail.com',
                password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
                firstName: 'Driver ',
                lastName: 'driver',
                phoneNumber: '+48500000000',
                status: 'active'
            });


            const client = new Client({
                _id: '5c83e9323380f56779712e7d',
                email: 'client@gmail.com',
                password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
                firstName: 'Client ',
                lastName: 'client',
                phoneNumber: '+48500000000',
                status: 'active'
            });

            const vehicle = new Vehicle({
                _id: "5c83e9323380f56779712e7e",
                make: 'BMW',
                yearOfManufacture: '2010',
                photos: [],
                status: 'active'
            });

            const transportEvent = new TransportEvent({
                title: 'Deliver basketball team',
                description: 'Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce a quam. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Vestibulum volutpat pretium libero. Cras dapibus. Aliquam eu nunc. Proin faucibus arcu quis ante. Vestibulum volutpat pretium libero. Aliquam eu nunc.',
                locationFromCoordinates: {
                    lat: 52.203992,
                    lng: 21.013844,
                    description: "Sapaya. Restauracja azjatycka"
                },
                locationToCoordinates: {
                    lat: 52.167373,
                    lng: 20.967948,
                    description: "Warsaw Chopin Airport"
                },
                dateStart: "2019-03-10T10:45:00.000Z",
                dateEnd: "2019-03-10T13:15:00.000Z",
                drivers: ["5c83e9323380f56779712e7f"],
                clients: ["5c83e9323380f56779712e7d"],
                vehicle: "5c83e9323380f56779712e7e",
                createdAt: "2019-03-06T119:23:00.000Z",
                modifiedAt: "2019-03-06T19:24:00.000Z",
                status: "paid, finished"
            });

            Promise.all([
                admin.save(),
                driver.save(),
                client.save(),
                vehicle.save(),
                transportEvent.save()
            ]).then( () => {
                console.log('db success')
                process.exit();
                }
            ).catch((err) => {
                console.log("Connection failed!");
                console.log(err)
            });
        });
    });
*/
