export const environment = {
  production: true,
  url: 'https://murmuring-meadow-21822.herokuapp.com',
  host:'https://murmuring-meadow-21822.herokuapp.com/api',
  API: `AIzaSyAB_7LWDOVpizUdAsFcQl4JLvFZgfOdqqo`,
  translateUrl: './frontend/assets/i18n/',
  assetsUrl: '/frontend/assets'
};
