import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from './guard/RoleGuard';
import { LoginComponent } from './components/login/login.component';

const appRoutes: Routes = [
	{
		path: 'admin',
		loadChildren: './modules/admin/admin.module#AdminModule',
		canActivate: [ RoleGuard ],
		data: {
			role: 'admin'
		}
	},
	{
		path: 'driver',
		loadChildren: './modules/driver/driver.module#DriverModule',
		canActivate: [RoleGuard],
		data: {
			role: 'driver'
		}
	},
	{
		path: 'client',
		loadChildren: './modules/client/clients.module#ClientsModule',
		canActivate: [RoleGuard],
		data: {
			role: 'client'
		}
	},
	{
		path: 'login',
		component: LoginComponent,
		canActivate: [ RoleGuard ]
	},

	{ path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(appRoutes);
