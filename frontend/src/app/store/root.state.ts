import { TransportEvent } from "src/app/interfaces/TransportEvent";
import { LoggedUser } from "src/app/interfaces/LoggedUser";

export interface RootState {
    transportEvents: TransportEvent[];
    user: LoggedUser;
}