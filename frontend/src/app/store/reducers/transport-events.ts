import { ActionTypes, ActionsUnion } from '../actions/transport-events.actions';
import { TransportEvent } from 'src/app/interfaces/TransportEvent';

const initialState = [] as TransportEvent[];

export function transportEventsReducer(state = initialState, action: ActionsUnion) {
	switch (action.type) {
        case ActionTypes.SET: {
            state = action.payload;
            return state;
        }
        case ActionTypes.ADD: {
            state.push(action.payload);
            return state;
        }
        case ActionTypes.EDIT: {
            let transportEventIndex = state.findIndex(transportEvent => transportEvent._id === action.payload.id);
            if(transportEventIndex !== -1) {
                state.splice(transportEventIndex, 1, action.payload.transportEvent);
            }
            return state;
        }
        case ActionTypes.REMOVE: {
            state = state.filter(transportEvent => transportEvent._id !== action.payload);
			return state;
        }
		default:
			return state;
	}
}