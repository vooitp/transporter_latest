import { ActionTypes, ActionsUnion } from '../actions/user.actions';
import { LoggedUser } from 'src/app/interfaces/LoggedUser';

const initialState = null as LoggedUser;

export function userReducer(state = initialState, action: ActionsUnion) {
	switch (action.type) {
        case ActionTypes.SET: {
            state = action.payload;
            localStorage.setItem('currentUser', JSON.stringify(state));
            return state;
        }
        case ActionTypes.CLEAR: {
            state = null;
            localStorage.removeItem('currentUser');
			return state;
        }
		default:
			return state;
	}
}