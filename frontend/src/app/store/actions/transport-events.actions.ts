import { Action } from '@ngrx/store';
import { TransportEvent } from 'src/app/interfaces/TransportEvent';
 
export enum ActionTypes {
    SET = 'SET TRANSPORT EVENTS',
    ADD = 'ADD TRANSPORT EVENT',
    EDIT = 'EDIT TRANSPORT EVENT',
    REMOVE = 'REMOVE TRANSPORT EVENT'
}
 
export class Set implements Action {
  readonly type = ActionTypes.SET;

  constructor(public payload: TransportEvent[]) {}
}
 
export class Add implements Action {
  readonly type = ActionTypes.ADD;

  constructor(public payload: TransportEvent) {}
}
 
export class Edit implements Action {
  readonly type = ActionTypes.EDIT;

  constructor(public payload: {transportEvent: TransportEvent, id: string}) {}
}

export class Remove implements Action {
    readonly type = ActionTypes.REMOVE;

    constructor(public payload: string) {}
}

export type ActionsUnion = Set | Add | Edit | Remove;