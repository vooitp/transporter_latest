import { Action } from '@ngrx/store';
import { LoggedUser } from 'src/app/interfaces/LoggedUser';
 
export enum ActionTypes {
    SET = 'SET USER',
    CLEAR = 'CLEAR USER'
}
 
export class Set implements Action {
  readonly type = ActionTypes.SET;

  constructor(public payload: LoggedUser) {}
}

export class Clear implements Action {
    readonly type = ActionTypes.CLEAR;
}

export type ActionsUnion = Set | Clear;