import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class RoleGuard implements CanActivate {
	constructor(public authService: AuthService, public router: Router) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const currentUserRole = this.authService.getUserRole();

		const expectedRole = route.data.role;

		if(state.url === '/login' && currentUserRole) {
			this.router.navigate([currentUserRole]);
			return false;
		} else if(!currentUserRole && state.url !== '/login' && state.url.indexOf('register') === -1) {
			this.router.navigate(['login']);
			return false;
		} else if(currentUserRole && currentUserRole !== expectedRole) {
			this.router.navigate([currentUserRole]);
			return false;
		}

		return true;
	}
}