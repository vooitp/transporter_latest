import {NgModule} from "@angular/core";
import {AppDateAdapter, APP_DATE_FORMATS} from './utils/date-adapter';
import {
  MatDialogModule,
  MatButtonModule,
  MatTooltipModule,
  MatCardModule,
  MatInputModule,
  MatTableModule,
  MatToolbarModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatRippleModule,
  MatExpansionModule,
  MatSelectModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSidenavModule,
  MatListModule,
  DateAdapter,
  MAT_DATE_FORMATS
} from '@angular/material';


const modules = [
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatTooltipModule,
  MatInputModule,
  MatTableModule,
  MatToolbarModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatRippleModule,
  MatExpansionModule,
  MatSelectModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSidenavModule,
  MatListModule
]

@NgModule({
  imports: [...modules],
  exports: [...modules],
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class CustomMaterialModule {
}
