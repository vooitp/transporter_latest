import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Rx";
import {Invitation} from "../interfaces/Invitation";
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {
  public baseUrl = environment.host;

  constructor(private http: HttpClient) {
  }

  sendInvitation(invitationObj: Invitation): Observable<any> {
    return this.http.post(this.baseUrl + "/invitation", invitationObj)
  }

  getInviations() {
    return <Observable<Invitation[]>> this.http.get(this.baseUrl + "/invitation");
  }

  deleteInvitation(id) {
    return this.http.delete(`${this.baseUrl}/invitation/${id}`);
  }
}
