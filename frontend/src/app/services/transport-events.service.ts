import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {checkIfDatesSameDay, getDayDifference} from '../utils/date';
import {environment} from "../../environments/environment";
import {TransportEvent} from "../interfaces/TransportEvent" ;

@Injectable({
  providedIn: 'root'
})
export class TransportEventsService {

  public baseUrl = `${environment.url}/api`;

  constructor(private http: HttpClient) {
  }

  get(fromDate: Date | null, toDate: Date | null, limit?: number) {
    const fromParam = fromDate ? `fromDate=${fromDate.toISOString()}` : '';
    const toParam = toDate ? `toDate=${toDate.toISOString()}` : '';
    const limitParam = limit ? `limit=${limit}` : '';
    const params = [fromParam, toParam, limitParam].filter(param => !!param).join('&');
    return this.http
      .get(
        `${this.baseUrl}/transport-event?${params}`
      )
      .map((data: TransportEvent[]) => {
        const events = [];
        for (const event of data) {
          event.dateStart = new Date(event.dateStart);
          event.dateEnd = new Date(event.dateEnd);
          events.push(event);
          if (!checkIfDatesSameDay(event.dateStart, event.dateEnd)) {
            event.multiday = true;
            const dayDifference = getDayDifference(event.dateStart, event.dateEnd) + 1;
            event.multidayTitle = `${event.title} (Day 1/${dayDifference})`;

            for (let i = 1; i < dayDifference; i++) {
              const copyEvent: TransportEvent = JSON.parse(JSON.stringify(event));
              copyEvent.multidayMainRef = event;
              copyEvent.multidayTitle = `${copyEvent.title} (Day ${i + 1}/${dayDifference})`;
              copyEvent._id += '/' + i;
              copyEvent.dateStart = new Date(event.dateStart);
              copyEvent.dateEnd = new Date(event.dateEnd);
              copyEvent.dateStart.setDate(event.dateStart.getDate() + i);
              events.push(copyEvent);
            }
          }
        }
        return events;
      })
  }

  getById(id: string) {
    return this.http.get(`${this.baseUrl}/transport-event/${id}`).map((data: TransportEvent) => {
      data.dateStart = new Date(data.dateStart);
      data.dateEnd = new Date(data.dateEnd);
      return data;
    });
  }

  add(transportEvent: TransportEvent): Observable<any> {
    return this.http.post(`${this.baseUrl}/transport-event`, transportEvent);
  }

  edit(transportEvent: TransportEvent, id: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/transport-event/${id}`, transportEvent);
  }

  remove(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/transport-event/${id}`);
  }
}
