import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs/Rx";
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GmapsService {

  private url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=`;

  constructor(private http: HttpClient) {
  }

  getPlaces(params: string): Observable<any> {
    return this.http.get(`${this.url}${params}&key=${environment.API}`)
  }
}


