import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {Client} from "../interfaces/Client";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private domain = `${environment.url}`;
  private url = `${this.domain}/api/client`;

  constructor(private  http: HttpClient) {
  }

  registerNewClient(data: Client, token): Observable<any> {
    return this.http.post(`${this.url}/register/${token}`, data);
  }

  getClients() {
    return this.http.get(`${this.url}`).map(data => <Client[]> data);
  }

  getClientById(id: number) {
    return this.http.get(`${this.url}/${id}`);
  }

  deleteClientById(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }

  updateClientById(id: number, obj: Client): Observable<any> {
    return this.http.put(`${this.url}/${id}`, obj);
  }
}

