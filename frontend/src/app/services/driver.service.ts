import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {Driver} from "../interfaces/Driver";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  private domain = `${environment.url}`;
  private url = `${this.domain}/api/driver`;

  constructor(private  http: HttpClient) {
  }

  registerNewDriver(driverData: Driver, token): Observable<any> {
    return this.http.post(`${this.url}/register/${token}`, driverData);
  }

  getDrivers(): Observable<any> {
    return this.http.get(this.url);
  }

  deleteDriver(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }


  getDriverById(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  updateDriverById(id: number, obj: Driver): Observable<any> {
    return this.http.put(`${this.url}/${id}`, obj)
  }
}
