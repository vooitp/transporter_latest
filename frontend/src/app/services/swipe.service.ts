import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs/Rx";

type Swipe = 'left' | 'right' | 'up' | 'down';

@Injectable({
  providedIn: 'root'
})
export class SwipeService {
    detectSwipe(target: HTMLElement, events: Array<Swipe> | Swipe): Observable<Swipe> {
        const subject = new Subject<Swipe>();
        let swipedir;
        let startX;
        let startY;
        let distX;
        let distY;
        let threshold = 100;
        let restraint = 100;
        let allowedTime = 300;
        let elapsedTime;
        let startTime;

        target.addEventListener('touchstart', e => {
            let touchobj = e.changedTouches[0];
            swipedir = 'none';
            distX = 0;
            distY = 0;
            startX = touchobj.pageX;
            startY = touchobj.pageY;
            startTime = new Date().getTime();
        }, false);

        target.addEventListener('touchend', e => {
            let touchobj = e.changedTouches[0];
            distX = touchobj.pageX - startX;
            distY = touchobj.pageY - startY ;
            elapsedTime = new Date().getTime() - startTime;
            if (elapsedTime <= allowedTime) {
                if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) {
                    swipedir = (distX < 0)? 'left' : 'right';
                }
                else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) {
                    swipedir = (distY < 0)? 'up' : 'down';
                }
            }

            if((typeof events === 'object' && events.indexOf(swipedir) !== -1) ||
                (typeof events === 'string' && events === swipedir)) {
                subject.next(swipedir);
            }
        }, false);

        return subject.asObservable();
    }
}
