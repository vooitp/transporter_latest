import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {User} from '../interfaces/User';
import {map} from 'rxjs/operators';
import {LoggedUser} from '../interfaces/LoggedUser';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private urlAuth = `${environment.url}/api/login`;
  private loggedUser: LoggedUser = null;

  constructor(private http: HttpClient) {
    const savedUserData = localStorage.getItem('currentUser');
    if (savedUserData) {
      this.loggedUser = JSON.parse(savedUserData);
    }
  }

  getUser() {
    return this.loggedUser;
  }

  getUserRole() {
    if (this.loggedUser) {
      return this.loggedUser.role;
    }
    return null;
  }

  login(User: User): Observable<any> {
    return this.http.post(this.urlAuth, User).pipe(
      map((user: LoggedUser) => {
        if (user) {
          this.loggedUser = user;
          localStorage.setItem('currentUser', JSON.stringify(this.loggedUser));
        }
        return user;
      })
    );
  }

  logout() {
    this.loggedUser = null;
    localStorage.removeItem('currentUser');
  }

  isLoggedIn() {
    return !!this.loggedUser;
  }
}
