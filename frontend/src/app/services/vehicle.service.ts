import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {Vehicle} from "../interfaces/Vehicle";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  private domain = `${environment.url}`;
  private url = `${this.domain}/api/vehicle`;

  constructor(private http: HttpClient) {
  }

  createNewCar(carObj): Observable<any> {
    return this.http.post(this.url, carObj);
  }

  getVehicles(): Observable<any> {
    return this.http.get(this.url);
  }

  getVehicleById(id): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  updateSingleVehicle(data, id): Observable<any> {
    return this.http.put(`${this.url}/${id}`, data);
  }

  deleteVehicle(id) {
    return this.http.delete(`${this.url}/${id}`);
  }

}
