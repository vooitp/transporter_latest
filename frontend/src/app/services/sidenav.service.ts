import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Subject} from 'rxjs/Subject';


@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private clickToggleEventStream = new Subject<any>();
  public clickToggleObservable = this.clickToggleEventStream.asObservable();

  constructor() {
  }

  emitStream(state) {
    this.clickToggleEventStream.next(state);
  }
}
