import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {Driver} from "../interfaces/Driver";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private domain = `${environment.url}`;
  private url = `${this.domain}/api/driver`;

  constructor(private  http: HttpClient) {
  }



}
