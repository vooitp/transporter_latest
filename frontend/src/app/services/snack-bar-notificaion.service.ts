import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material";

@Injectable({
  providedIn: 'root'
})
export class SnackBarNotificaionService {


  constructor(private snackBar: MatSnackBar) {
  }

  openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 3000
    });
  }

  triggerSnackbar(message: string, action?: string) {
    this.openSnackBar(message, action)
  }


}
