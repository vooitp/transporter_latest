import {FormControl, ValidatorFn, AbstractControl} from "@angular/forms";

export function EmailValidator(input: FormControl) {
  let inputValue = input.value;
  if (inputValue.indexOf("@") === -1) {
    return {emailValidationError: true};

  }
  return null;
}

export class PasswordValidation {
  static MatchPassword(abstractControl: AbstractControl): ValidatorFn {
    let password = abstractControl.get('password').value; // to get value in input tag
    let confirmPassword = abstractControl.get('retypePassword').value; // to get value in input tag
    if (password != confirmPassword) {
      abstractControl.get('retypePassword').setErrors({MatchPassword: true})
    } else {
      return null
    }
  }
}
