export const TransportEventStatus = {
    PAID_FINISHED: 'paid, finished',
    UNPAID_FINISHED: 'unpaid, finished, invoicedd',
    FINISHED_UNINVOICED: 'finished, uninvoiced',
    FINISHED_SUBTRACTOR_TO_PAY: 'subcontractor finished, paid by cliend, subcontractor is to be paid'
};

export const TransportEventStatusColor = {
    [TransportEventStatus.PAID_FINISHED]: 'blue',
    [TransportEventStatus.UNPAID_FINISHED]: 'red',
    [TransportEventStatus.FINISHED_UNINVOICED]: 'yellow',
    [TransportEventStatus.FINISHED_SUBTRACTOR_TO_PAY]: 'green'
};