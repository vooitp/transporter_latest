import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	constructor(
		public router: Router,
		private authService: AuthService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const notAuthPaths = [ 'api/login', 'api/driver/register', 'https://maps.googleapis.com/maps/api' ];

		if (notAuthPaths.find(path => req.url.includes(path))) {
			return next.handle(req);
		} else {
			const currentUser = this.authService.getUser();
			let clonedRequest = req.clone();
			if(currentUser) {
				clonedRequest = req.clone({
					headers: req.headers.set('Authorization', `Bearer ${String(currentUser.token)}`)
				});
			}
			return next.handle(clonedRequest).pipe(
				catchError((error: HttpErrorResponse) => {
					if (error instanceof HttpErrorResponse) {
						if (error.status === 401) {
							this.authService.logout();
							this.router.navigate(['login']);
						}
					}
					return throwError(error);
			}));
		}
	}
}
