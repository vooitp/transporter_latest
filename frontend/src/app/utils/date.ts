export const checkIfDatesSameDay = (date1: Date, date2: Date) => {
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
};

export const getDayDifference = (dateStart: Date, dateEnd: Date) => {
  const dateStartCopy = new Date(dateStart);
  dateStartCopy.setHours(0, 0, 0, 0);
  const dateEndCopy = new Date(dateEnd);
  dateEndCopy.setHours(0, 0, 0, 0);
  const timeDiff = dateEndCopy.getTime() - dateStartCopy.getTime();
  const oneDay = 1000 * 60 * 60 * 24;
  return Math.round(timeDiff / oneDay);
}

export const milliseconds = (h, m, s) => ((h * 60 * 60 + m * 60 + s) * 1000);

export const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
export const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const weekDaysShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];


