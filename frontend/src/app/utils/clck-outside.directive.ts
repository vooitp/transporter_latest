import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';
import {SidenavService} from "../services/sidenav.service";

@Directive({
  selector: '[clickOutside]'
})
export class ClickOutsideDirective {

  constructor(private elementRef: ElementRef,
              private sidenavService: SidenavService,) {
  }

  @Output()
  public clickOutside = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      console.log('clicked outsisde')
      this.clickOutside.emit(null);
      this.sidenavService.emitStream(false);
    }
  }
}
