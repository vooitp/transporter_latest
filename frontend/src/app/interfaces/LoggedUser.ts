export interface LoggedUser {
  token: string;
  role: string;
}
