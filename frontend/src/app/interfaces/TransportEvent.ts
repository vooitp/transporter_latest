import {Driver} from "./Driver";
import {User} from "./User";
import {Vehicle} from "./Vehicle";

export interface TransportEvent {
  _id: string;
  title: string;
  locationFromCoordinates: {
    lat: number,
    lng: number,
    description: string
  };
  locationToCoordinates: {
    lat: number,
    lng: number,
    description: string
  };
  dateStart: Date;
  dateEnd: Date;
  drivers: Array<Driver>;
  clients: Array<User>;
  vehicle: Vehicle;
  createdAt?: string;
  modifiedAt?: string;

  multiday?: boolean;
  multidayTitle?: string;
  multidayMainRef?: TransportEvent;
  position?: number;
  offsetTop?: number;
}