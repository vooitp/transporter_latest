export interface Vehicle {
  _id?: string;
  make: string;
  yearOfManufacture: string;
  photos: string[];
  status: string;
}
