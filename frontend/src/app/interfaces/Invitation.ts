export interface Invitation {
  email: string;
  userRole: string;
  expirationDate?: Date;
  token?:string | number;
  _id?:number;
}
