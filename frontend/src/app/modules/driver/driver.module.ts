import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DriverRoutingModule} from './driver-routing.module';
import {SharedModule} from "../shared.module";
import {DriverService} from "../../services/driver.service";
import {VehicleService} from "../../services/vehicle.service";

@NgModule({
  imports: [
    CommonModule,
    DriverRoutingModule,
    SharedModule
  ],
  exports: [
  ],
  providers:[VehicleService, DriverService,]
})
export class DriverModule {
}
