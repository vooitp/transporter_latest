import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegisterComponent} from "../../components/register/register.component";
import {CalendarComponent} from "../../components/calendar/calendar.component";


const routes: Routes = [
  {path: '', redirectTo: 'calendar'},
  {path: 'register/:token/:email', component: RegisterComponent},

  {path: 'driver/:id', component: RegisterComponent},
  {path: 'calendar', component: CalendarComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class DriverRoutingModule {
}

