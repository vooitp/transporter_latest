import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientsRoutingModule} from './clients-routing.module';
import {SharedModule} from "../shared.module";
import {ClientService} from "../../services/client.service";
import {CustomMaterialModule} from 'src/app/core.module';

@NgModule({
  imports: [
    CommonModule,
    ClientsRoutingModule,
    SharedModule,
    CustomMaterialModule
  ],
  declarations: [],
  providers: [ClientService]
})
export class ClientsModule {
}
