import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegisterComponent} from "../../components/register/register.component";
import { CalendarComponent } from 'src/app/components/calendar/calendar.component';

const routes: Routes = [
  {path: '', redirectTo: 'calendar'},
  {path: 'register/:token/:email', component: RegisterComponent},
  {path: 'calendar', component: CalendarComponent},
  ]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
