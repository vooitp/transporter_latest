import {Component, OnInit} from '@angular/core';
import {SidenavService} from '../../../services/sidenav.service';
import {AuthService} from 'src/app/services/auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  isExpanded = false;
  public adminMenuItems = [
    {
      icon: 'calendar_today',
      title: 'SideNav.calendar_view',
      link: '/admin/calendar',
      tooltip: !this.isExpanded ? 'SideNav.calendar_view' : ''
    },
    {
      icon: 'airline_seat_recline_normal',
      title: 'SideNav.drivers_list',
      link: '/admin/drivers',
      tooltip: !this.isExpanded ? 'SideNav.drivers_list' : ''
    },
    {
      icon: 'supervised_user_circle',
      title: 'SideNav.clients_list',
      link: '/admin/clients',
      tooltip: !this.isExpanded ? 'SideNav.clients_list' : ''
    },
    {
      icon: 'local_shipping',
      title: 'SideNav.vehicles_list',
      link: '/admin/vehicles',
      tooltip: !this.isExpanded ? 'SideNav.vehicles_list' : ''
    },
    {
      icon: 'alternate_email',
      title: 'SideNav.invitations_list',
      link: '/admin/invitations',
      tooltip: !this.isExpanded ? 'SideNav.invitations_list' : ''
    }
  ];

  public clientMenuItems = [
    {
      icon: 'calendar_today',
      title: 'SideNav.calendar_view',
      link: '/admin/calendar',
      tooltip: !this.isExpanded ? 'SideNav.calendar_view' : ''
    }
  ];

  public driverMenuItems = [
    {
      icon: 'calendar_today',
      title: 'SideNav.calendar_view',
      link: '/admin/calendar',
      tooltip: !this.isExpanded ? 'SideNav.calendar_view' : ''
    }
  ];

  constructor(private sidenavService: SidenavService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.sidenavService.clickToggleObservable.subscribe((state) => {
      this.isExpanded = state;
    });
  }

  get isLoggedIn() {
    return this.authService.isLoggedIn();
  }



  public closeSideBar() {
    this.sidenavService.emitStream(false);
  }


  logout() {
    this.authService.logout();
    this.sidenavService.emitStream(false);
    this.router.navigate(['/login']);
  }

  get menuItems() {
    const role = this.authService.getUserRole();
    if (role === 'admin') {
      return this.adminMenuItems;
    } else if (role === 'client') {
      return this.clientMenuItems;
    } else if (role === 'driver') {
      return this.driverMenuItems;
    }
    return [];
  }
}
