import {Component} from '@angular/core';

@Component({
  selector: 'back-route-button',
  templateUrl: './back-route-button.component.html',
  styleUrls: ['./back-route-button.component.scss']
})
export class BackRouteButtonComponent {

  constructor() {
  }


  navigateBackHistory() {
    window.history.back();
  }

}
