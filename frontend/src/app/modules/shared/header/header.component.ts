import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {SidenavService} from "../../../services/sidenav.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  isExpanded: boolean = false;
  supportedLangs = [];
  selectedLang = 'en';

  constructor(private router: Router,
              private authService: AuthService,
              private sidenavService: SidenavService,
              private translate: TranslateService) {
    this.selectedLang = this.translate.currentLang;
    this.supportedLangs = this.translate.langs;
  }

ngOnInit(){
  this.sidenavService.clickToggleObservable.subscribe((state) => {
    this.isExpanded = state;
  });

}
  get userRole() {
    return this.authService.getUserRole();
  }

  get isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  useLanguage(language: string) {
    this.selectedLang = language;
    this.translate.use(language);
  }


  toggleSideNav() {
    this.sidenavService.emitStream(!this.isExpanded);
  }
}
