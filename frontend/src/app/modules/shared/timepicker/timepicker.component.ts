import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";

//https://www.npmjs.com/package/ngx-material-timepicker
@Component({
  selector: 'timepicker',
  template: `
    <i class="material-icons" (click)="openTimePicker()">access_time</i>
    <input matInput [ngxTimepicker]="timePicker" [value]="value" [format]="24" readonly>
    <ngx-material-timepicker #timePicker (timeSet)="setTime($event)"></ngx-material-timepicker>
  `,
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent {
  @Input() formCtrl: FormControl;

  @ViewChild('timePicker') timePicker;

  get value() {
    const date: Date = this.formCtrl.value;
    const hours = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
    const minutes = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
    return `${hours}:${minutes}`;
  }

  openTimePicker() {
    this.timePicker.isOpened = true;
  }

  setTime(time: string) {
    const hours = parseInt(time.split(':')[0]);
    const minutes = parseInt(time.split(':')[1]);
    const date: Date = this.formCtrl.value;
    date.setHours(hours);
    date.setMinutes(minutes);
    this.formCtrl.setValue(date);
  }
}
