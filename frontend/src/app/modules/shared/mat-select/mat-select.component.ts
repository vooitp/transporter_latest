import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mat-select',
  templateUrl: './mat-select.component.html',
  styleUrls: ['./mat-select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() data;

  constructor() {
  }

  ngOnInit() {
    console.log(this.data)
  }

}
