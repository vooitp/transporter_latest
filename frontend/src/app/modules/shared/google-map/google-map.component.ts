import {Component, AfterViewInit, ElementRef, Output, EventEmitter, Input, OnChanges, OnInit} from '@angular/core';

declare const google: any;

@Component({
  selector: 'google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class GoogleMapComponent implements OnInit, AfterViewInit, OnChanges {
  private map = null;
  private autocomplete = null;
  private marker = null;

  @Input() placeholder = 'Enter location';
  @Input() coordinates;
  @Input() demonstration = false;

  @Output() placeSelected = new EventEmitter();

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initMap();
    if(!this.demonstration) {
      this.initAutocomplete();
    }
  }


  ngOnChanges(change) {
    if (
      this.map &&
      change && change.coordinates &&
      change.coordinates.currentValue.lat &&
      change.coordinates.currentValue.lng
    ) {
      this.setMarkerAndCenterMap();
    }
  }

  initMap() {
    const mapElem = this.elementRef.nativeElement.querySelector('.map');
    this.map = new google.maps.Map(mapElem, {
      center: {lat: -34.397, lng: 150.644},
      zoom: 10
    });
    this.setMarkerAndCenterMap();
  }

  initAutocomplete() {
    const input = this.elementRef.nativeElement.querySelector('input');
    this.autocomplete = new google.maps.places.Autocomplete(input, {});
    google.maps.event.addListener(this.autocomplete, 'place_changed', () => {

      const place = this.autocomplete.getPlace();

      if (!place.geometry || !place.geometry.viewport) {
        alert('not valid coordinates');
        return;
      }
      this.emitCurrentPosition(place.geometry.location.lat(), place.geometry.location.lng(), place.formatted_address);
    });
  }

  preventSubmit(event: KeyboardEvent) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  }

  setMarkerAndCenterMap() {
    const {lat, lng, description} = this.coordinates;
    const latlng = new google.maps.LatLng(lat, lng);
    if (this.marker) {
      this.marker.setPosition(latlng)
    } else {
      this.marker = new google.maps.Marker({
        position: latlng,
        draggable: !this.demonstration,
        map: this.map,
        title: description
      });

      google.maps.event.addListener(this.marker, 'dragend', () => {
        const position = this.marker.getPosition();
        this.emitCurrentPosition(position.lat(), position.lng(), '');
      });
    }
    this.map.setCenter(latlng);
  }

  emitCurrentPosition(lat, lng, description) {
    this.placeSelected.emit({
      lat,
      lng,
      description
    });
  }

  onDescriptionChange(event: KeyboardEvent) {
    this.emitCurrentPosition(this.coordinates.lat, this.coordinates.lng, (event.target as HTMLInputElement).value);
  }

}

