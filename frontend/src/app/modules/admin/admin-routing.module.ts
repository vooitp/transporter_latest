import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CarFormComponent} from './car-form/car-form.component';
import {CalendarComponent} from 'src/app/components/calendar/calendar.component';
import {EventComponent} from '../../components/event/event.component';
import {VehiclesComponent} from './vehicles/vehicles.component';
import {DriversComponent} from './drivers/drivers.component';
import {ClientsComponent} from './clients/clients.component';
import {InvitationsComponent} from './invitations/invitations.component';
import {DriverFormComponent} from "./driver-form/driver-form.component";


const routes: Routes = [
  {path: '', redirectTo: 'calendar'},
  {path: 'drivers', component: DriversComponent},
  {path: 'driver', component: DriverFormComponent},
  {path: 'driver/:id/:role', component: DriverFormComponent},
  {path: 'clients', component: ClientsComponent},
  {path: 'client', component: DriverFormComponent},
  {path: 'client/:id/:role', component: DriverFormComponent},
  {path: 'invitations', component: InvitationsComponent},
  {path: 'vehicles', component: VehiclesComponent},
  {path: 'vehicle', component: CarFormComponent},
  {path: 'vehicle/:id', component: CarFormComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'calendar/new-event', component: EventComponent},
  {path: 'calendar/new-event/:date', component: EventComponent},
  {path: 'calendar/event/:id', component: EventComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
