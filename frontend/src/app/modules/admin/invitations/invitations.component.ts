import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import {InvitationService} from 'src/app/services/invitation.service';
import {SnackBarNotificaionService} from 'src/app/services/snack-bar-notificaion.service';
import {ModalConfirmationComponent} from '../../shared/modal-confirmation/modal-confirmation.component';
import {ModalInvitationComponent} from '../modals/modal/modalInvitation.component';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss']
})
export class InvitationsComponent implements OnInit {
  invitationColumns: string[] = ['Email', 'Role', 'Actions'];
  invitationColumnsMap = {
    Email: 'email',
    Role: 'userRole'
  };
  invitationSource: MatTableDataSource<any>;
  actions = [
    {id: 'delete', icon: 'remove', color: 'danger', title: 'Delete'}
  ];

  constructor(private invitationService: InvitationService,
              private dialog: MatDialog,
              private snackBarNotificaionService: SnackBarNotificaionService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.loadInvitations();
  }

  loadInvitations() {
    this.invitationService.getInviations().subscribe(invitations => {
      this.invitationSource = new MatTableDataSource(invitations);
    })
  }

  handleAction({id, data}: { id: string, data: any }) {
    switch (id) {
      case 'delete': {
        this.deleteConfirmationDialog(data);
        break;
      }
      case 'add': {
        this.openNewInvitationDialog();
      }
    }
  }

  deleteConfirmationDialog(data) {

    const payload = {title: `Confirm deleting invitation for:\n${data.email}`};

    const dialogRef = this.dialog.open(ModalConfirmationComponent, {
      width: '320px',
      data: payload
    });

    dialogRef.afterClosed().subscribe(() => {
        this.invitationService.deleteInvitation(data._id).subscribe(() => {
          this.loadInvitations();
          this.snackBarNotificaionService.triggerSnackbar('Invitation has been removed succesfully');
        });
    });
  }

  openNewInvitationDialog() {
    const dialogRef = this.dialog.open(ModalInvitationComponent, {
      width: '300px',
      height: '280px'
    });
    dialogRef.afterClosed().subscribe((result) => {
        this.invitationService.sendInvitation(result).subscribe(() => {
          this.translate.get('Notifications.invitation_success').subscribe((translated: string) => {
            this.snackBarNotificaionService.triggerSnackbar(
              translated
            );
          });
          this.loadInvitations();
        });
    });
  }
}
