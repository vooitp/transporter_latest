import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { VehicleService } from 'src/app/services/vehicle.service';
import { SnackBarNotificaionService } from 'src/app/services/snack-bar-notificaion.service';
import { ModalConfirmationComponent } from '../../shared/modal-confirmation/modal-confirmation.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-vehicles',
    templateUrl: './vehicles.component.html',
    styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
    vehicleColumns: string[] = [ 'Make', 'Year', 'Status', 'Actions' ];
    vehicleColumnsMap = {
        Make: 'make',
        Year: 'yearOfManufacture',
        Status: 'status'
    }
    vehicleSource: MatTableDataSource<any>;
    actions = [
        {id: 'edit', icon: 'edit', color: 'info', title: 'Edit'},
        {id: 'delete', icon: 'remove', color: 'danger', title: 'Delete'}
    ];
    
    constructor(
        private router: Router,
        private vehicleService: VehicleService,
        private dialog: MatDialog,
		private snackBarNotificaionService: SnackBarNotificaionService) {
    }

    ngOnInit() {
        this.loadVehicles();
    }

    loadVehicles() {
        this.vehicleService.getVehicles().subscribe(vehicles => {
            this.vehicleSource = new MatTableDataSource(vehicles);
        })
    }

    handleAction({id, data}: {id: string, data: any}) {
        switch(id) {
            case 'delete': {
                this.deleteConfirmationDialog(data);
                break;
            }
            case 'edit': {
                this.router.navigateByUrl(`/admin/vehicle/${data._id}`);
                break;
            }
        }
    }

    deleteConfirmationDialog(data) {
		const payload = { title: `Confirm deleting vehicle:\n${data.make} of ${data.yearOfManufacture}` };

		const dialogRef = this.dialog.open(ModalConfirmationComponent, {
			width: '300px',
			data: payload
		});

		dialogRef.afterClosed().subscribe((result) => {
			if(result) {
				this.vehicleService.deleteVehicle(data._id).subscribe(() => {
                    this.loadVehicles();
                    this.snackBarNotificaionService.triggerSnackbar('Vehicle has been removed succesfully');
                });
			}
		});
	}
}