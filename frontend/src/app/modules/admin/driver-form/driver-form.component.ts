import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientService} from 'src/app/services/client.service';
import {DriverService} from 'src/app/services/driver.service';
import {SnackBarNotificaionService} from 'src/app/services/snack-bar-notificaion.service';
import {EmailValidator, PasswordValidation} from 'src/app/utils/customValidators';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-driver-form',
  templateUrl: './driver-form.component.html',
  styleUrls: ['./driver-form.component.scss']
})
export class DriverFormComponent implements OnInit {
  private token: string;
  private email: string;
  public id: number;
  public form: FormGroup;
  public submitted = false;
  public role: string;
  public data;
  public editMode = false;
  public header: Promise<any>;
  public registrationMode: boolean;
  public hide = true;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private clientService: ClientService,
              private driverService: DriverService,
              private snackBarNotificaionService: SnackBarNotificaionService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.initForm();

    this.route.params.subscribe((params) => {
      if (params['token'] && params['email']) {
        this.token = params['token'];
        this.email = params['email'];
        this.form.get('email').setValue(this.email);
        this.registrationMode = true;
        this.translateHeader('User_form.registration_header');
      }
      if (params['id']) {
        this.id = params['id'];
        if (params['role'] === 'client') {
          this.clientService.getClientById(this.id).subscribe((res) => {
            this.data = res;
            this.role = 'client';
            this.initForm();
            this.translateHeader('User_form.client_header');
            this.parseData(this.data, this.form);
            this.handleControlsAbility(this.form, this.editMode);
          });
        }
        if (params['role'] === 'driver') {
          this.driverService.getDriverById(this.id).subscribe((res) => {
            this.data = res;
            this.role = 'driver';
            this.initForm();
            this.translateHeader('User_form.driver_header');
            this.parseData(this.data, this.form);
            this.handleControlsAbility(this.form, this.editMode);
          });
        }
        // this.form.get('password').setValue('');
        // this.form.get('retypePassword').setValue('');
      }
    });
  }


  translateHeader(translationElement: string) {
    this.translate.get(translationElement).subscribe((translation: Promise<string>) => {
      this.header = translation;
    })
  }

  parseData(data, formGroup) {
    let formKeys = Object.keys(formGroup.getRawValue());
    for (let key in formKeys) {
      formGroup.get(formKeys[key]).setValue(data[formKeys[key]]);
    }
  }

  handleControlsAbility(formGroup, editMode) {
    if (!editMode) {
      Object.keys(formGroup.controls).forEach((key) => {
        formGroup.controls[key].disable();
      });
    } else {
      Object.keys(formGroup.controls).forEach((key) => {
        formGroup.controls[key].enable();
      });
    }
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    if (!this.editMode) {
      Object.keys(this.form.controls).forEach((key) => {
        this.form.controls[key].disable();
      });
    } else {
      Object.keys(this.form.controls).forEach((key) => {
        this.form.controls[key].enable();
      });
    }
  }

  initForm() {
    if (!this.role) {
      this.form = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, EmailValidator]),
        phoneNumber: new FormControl('', Validators.required),
        status: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        retypePassword: new FormControl('', Validators.required)
      }, PasswordValidation.MatchPassword);
    } else {
      this.form = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        phoneNumber: new FormControl('', Validators.required),
      });

    }
  }

  getForm() {
    let form = this.form.getRawValue();
    delete form.retypePassword;
    return form;
  }

  submitForm(): void {
    this.submitted = true;
    if (this.form.valid) {
      if (this.router.url.includes('client')) {
        this.clientService.registerNewClient(this.getForm(), this.token).subscribe((res) => {
          this.translate.get('Notifications.can_login').subscribe((translated: string) => {
            this.snackBarNotificaionService.openSnackBar(translated);
            setTimeout(() => {
              this.router.navigateByUrl('/login');
            }, 2000);
          });
        });
      }
      if (this.router.url.includes('driver')) {
        this.driverService.registerNewDriver(this.getForm(), this.token).subscribe(() => {
          this.translate.get('Notifications.can_login').subscribe((translated: string) => {
            this.snackBarNotificaionService.openSnackBar(translated);
            setTimeout(() => {
              this.router.navigateByUrl('/login');
            }, 2000);
          });
        });
      }
    } else {
      this.translate.get('Notifications.fill_credentials').subscribe((translated: string) => {
        this.snackBarNotificaionService.openSnackBar(translated);
      });
    }
  }

  updateForm(): void {
    this.submitted = true;
    if (this.form.valid) {
      if (this.role === 'client') {
        this.clientService.updateClientById(this.id, this.getForm()).subscribe((res) => {
          this.snackBarNotificaionService.openSnackBar(
            `User ${this.getForm().firstName} ${this.getForm().lastName} updated successfully`
          );
          this.router.navigateByUrl('/admin');
        });
      }
      if (this.role === 'driver') {
        this.driverService.updateDriverById(this.id, this.getForm()).subscribe((res) => {
          this.snackBarNotificaionService.openSnackBar(
            `User ${this.getForm().firstName} ${this.getForm().lastName} updated successfully`
          );
          this.router.navigateByUrl('/admin');
        });
      }

    } else {
      this.translate.get('Notifications.fill_credentials').subscribe((translated: string) => {
        this.snackBarNotificaionService.openSnackBar(translated);
      });
    }
  }
}
