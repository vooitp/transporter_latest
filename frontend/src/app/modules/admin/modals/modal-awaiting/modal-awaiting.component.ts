import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SnackBarNotificaionService } from '../../../../services/snack-bar-notificaion.service';
import { InvitationService } from 'src/app/services/invitation.service';

@Component({
	selector: 'app-modal-awaiting',
	templateUrl: './modal-awaiting.component.html',
	styleUrls: [ './modal-awaiting.component.scss' ]
})
export class ModalAwaitingComponent {
	constructor(
		private invitationService: InvitationService,
		private snackBarNotificaionService: SnackBarNotificaionService,
		public dialogRef: MatDialogRef<ModalAwaitingComponent>,
		@Inject(MAT_DIALOG_DATA) public data
	) {}

	public invitedUsersList = this.data;

	deleteInvitation(item) {
		this.invitationService.deleteInvitation(item._id).subscribe(() => {
			this.updateList(item);
			this.snackBarNotificaionService.triggerSnackbar('Invitation has been removed succesfully');
		});
	}

	updateList(item) {
		this.invitedUsersList = this.invitedUsersList.filter((el) => el._id !== item._id);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
