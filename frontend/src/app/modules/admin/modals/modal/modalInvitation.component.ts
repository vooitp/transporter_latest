import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../../../utils/customValidators';

@Component({
	selector: 'app-modal',
	templateUrl: './modalInvitation.component.html',
	styleUrls: [ './modalInvitation.component.scss' ]
})
export class ModalInvitationComponent implements OnInit {
	public invitationForm: FormGroup;
	public submitted = false;

	constructor(public dialogRef: MatDialogRef<ModalInvitationComponent>) {}

	ngOnInit() {
		this.invitationForm = new FormGroup({
			email: new FormControl('', [ Validators.required, EmailValidator ]),
			userRole: new FormControl('', Validators.required)
		});
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
