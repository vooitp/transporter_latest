import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import {DriverService} from 'src/app/services/driver.service';
import {SnackBarNotificaionService} from 'src/app/services/snack-bar-notificaion.service';
import {ModalConfirmationComponent} from '../../shared/modal-confirmation/modal-confirmation.component';
import {Router} from '@angular/router';
import {Driver} from 'src/app/interfaces/Driver';
import {TranslateService} from "@ngx-translate/core";
import 'rxjs/add/operator/switchMap';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})

export class DriversComponent implements OnInit {

  driverColumns: string[] = ['Name', 'Email', 'Number', 'Actions'];
  driverColumnsMap = {
    Name: (elem) => `${elem.firstName} ${elem.lastName}`,
    Email: 'email',
    Number: 'phoneNumber'
  }

  driverSource: MatTableDataSource<any>;

  actions = [
    {id: 'edit', icon: 'edit', color: 'info', title: 'Edit'},
    {id: 'delete', icon: 'remove', color: 'danger', title: 'Delete'}
  ];
  subscription: Subscription;

  constructor(private router: Router,
              private driverService: DriverService,
              private dialog: MatDialog,
              private snackBarNotificaionService: SnackBarNotificaionService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.loadDrivers();
  }

  loadDrivers() {
    this.driverService.getDrivers().subscribe(drivers => {
      this.driverSource = new MatTableDataSource(drivers);
    });
  }

  handleAction({id, data}: { id: string, data: Driver }) {
    switch (id) {
      case 'delete': {
        this.deleteConfirmationDialog(data);
        break;
      }
      case 'edit': {
        this.router.navigateByUrl(`/admin/driver/${data._id}/driver`);
        break;
      }
    }
  }

  triggeTranslatedSnackbar() {
    this.translate.get('Notifications.driver_removed').subscribe((translated: string) => {
      this.snackBarNotificaionService.triggerSnackbar(
        translated
      );
    });
  }

  async deleteConfirmationDialog(data) {
    let payload = await this.translate.get('Modals.delete_confirmation').toPromise();

    const dialogRef = this.dialog.open(ModalConfirmationComponent, {
      width: '300px',
      data: {title: `${payload}:\n${data.firstName} ${data.lastName}`}
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.driverService.deleteDriver(data._id).subscribe(() => {
          this.loadDrivers();
          this.triggeTranslatedSnackbar();
        });
      }
    });
  }

}
