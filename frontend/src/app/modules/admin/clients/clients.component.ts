import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';
import {ClientService} from 'src/app/services/client.service';
import {SnackBarNotificaionService} from 'src/app/services/snack-bar-notificaion.service';
import {ModalConfirmationComponent} from '../../shared/modal-confirmation/modal-confirmation.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  clientColumns: string[] = ['Name', 'Email', 'Number', 'Actions'];
  clientColumnsMap = {
    Name: (elem) => `${elem.firstName} ${elem.lastName}`,
    Email: 'email',
    Number: 'phoneNumber'
  };
  clientSource: MatTableDataSource<any>;
  actions = [
    {id: 'edit', icon: 'edit', color: 'info', title: 'Edit'},
    {id: 'delete', icon: 'remove', color: 'danger', title: 'Delete'}
  ];

  constructor(private router: Router,
              private clientService: ClientService,
              private dialog: MatDialog,
              private snackBarNotificaionService: SnackBarNotificaionService) {
  }

  ngOnInit() {
    this.loadClients();
  }

  loadClients() {
    this.clientService.getClients().subscribe(clients => {
      this.clientSource = new MatTableDataSource(clients);
    });
  }

  handleAction({id, data}: { id: string, data: any }) {
    switch (id) {
      case 'delete': {
        this.deleteConfirmationDialog(data);
        break;
      }
      case 'edit': {
        this.router.navigateByUrl(`/admin/client/${data._id}/client`);
        break;
      }
    }
  }

  deleteConfirmationDialog(data) {
    const payload = {title: `Confirm deleting client:\n${data.firstName} ${data.lastName}`};

    const dialogRef = this.dialog.open(ModalConfirmationComponent, {
      width: '300px',
      data: payload
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.clientService.deleteClientById(data._id).subscribe(() => {
          this.loadClients();
          this.snackBarNotificaionService.triggerSnackbar('Client has been removed succesfully');
        });
      }
    });
  }
}
