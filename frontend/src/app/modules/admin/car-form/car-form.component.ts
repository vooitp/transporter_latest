import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SnackBarNotificaionService} from '../../../services/snack-bar-notificaion.service';
import {VehicleService} from '../../../services/vehicle.service';
import {TranslateService} from "@ngx-translate/core";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.scss']
})
export class CarFormComponent implements OnInit {
  public id: string;
  public fetchedData;
  public submitted = false;
  public carForm: FormGroup;
  public imageUrls = [];
  public editMode = true;
  public viewMode: boolean;

  constructor(private route: ActivatedRoute,
              private vehicleService: VehicleService,
              private router: Router,
              private snackBarNotificaionService: SnackBarNotificaionService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.initForm();
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      if (!params['id']) {
        this.editMode = false;
      }

      if (params['id']) {
        this.vehicleService.getVehicleById(this.id).subscribe((res) => {
          this.viewMode = true;
          this.editMode = false;
          this.fetchedData = res;
          this.carForm.patchValue(this.fetchedData);
          this.handleControlsAbility(this.carForm, this.editMode);
        });
      }
    });
  }

  get photosArr() {
    return this.carForm.get('photos').value.map(photo => {
      if (photo.indexOf('base64') === -1 ) {
        return `${environment.url}${photo}`
      }
      return photo
    })
  }

  handleControlsAbility(formGroup, editMode) {
    if (!editMode) {
      Object.keys(formGroup.controls).forEach((key) => {
        formGroup.controls[key].disable();
      });
    } else {
      Object.keys(formGroup.controls).forEach((key) => {
        formGroup.controls[key].enable();
      });
    }
  }

  toggleEditMode() {
    this.viewMode = !this.viewMode;
    this.editMode = !this.editMode;
    if (!this.editMode) {
      Object.keys(this.carForm.controls).forEach((key) => {
        this.carForm.controls[key].disable();
      });
    } else {
      Object.keys(this.carForm.controls).forEach((key) => {
        this.carForm.controls[key].enable();
      });
    }
  }

  clearPhotosList() {
    this.imageUrls = [];
    this.carForm.get('photos').setValue('');
  }


  initForm() {
    this.carForm = new FormGroup({
      make: new FormControl('', Validators.required),
      yearOfManufacture: new FormControl('', Validators.required),
      photos: new FormControl([]),
      status: new FormControl('', Validators.required)
    });
  }


  onFileInput(event: any) {
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (e: any) => {
          // this.imageUrls.push(e.target.result); //<---saves fakepath...
          const photos = this.carForm.get('photos').value;
          photos.push(e.target.result)

          this.carForm.get('photos').setValue(photos);
        };
      }
    } else {
      event.target.value = '';
   }
  }

  getForm() {
    let form = this.carForm.getRawValue();
    return form;
  }

  submitForm(): void {
    this.submitted = true;
    if (this.carForm.valid) {
      this.vehicleService.createNewCar(this.getForm()).subscribe(
        (res) => {

          this.translate.get('Notifications.vehicle_added').subscribe((translated: string) => {
            this.snackBarNotificaionService.openSnackBar(translated);
            setTimeout(() => {
              this.router.navigateByUrl('/admin/vehicles');
            }, 3000);
          });
        },
        (error) => {
          this.translate.get('Notifications.vehicle_fails').subscribe((translated: string) => {
            this.snackBarNotificaionService.openSnackBar(translated);
          });


        }
      );
    } else {
      this.translate.get('Notifications.vehicle_fails').subscribe((translated: string) => {
        this.snackBarNotificaionService.openSnackBar(translated);
      });
    }
  }

  postForm(): void {
    this.submitted = true;
    if (this.carForm.valid) {
      this.vehicleService.updateSingleVehicle(this.getForm(), this.id).subscribe(() => {
        this.translate.get('Notifications.vehicle_update').subscribe((translated: string) => {
          this.snackBarNotificaionService.openSnackBar(translated);
        });
      });
    }
  }
}
