import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CustomMaterialModule} from "../../core.module";
import {ModalEditDriverComponent} from './modals/modal-edit-driver/modal-edit-driver.component';
import {SharedModule} from "../shared.module";
import {VehicleService} from "../../services/vehicle.service";
import {DriverService} from "../../services/driver.service";
import {EventService} from "../../services/event.service";
import {GmapsService} from "../../services/gmaps.service";
import {CarFormComponent} from './car-form/car-form.component';
import {VehiclesComponent} from './vehicles/vehicles.component';
import {DriversComponent} from './drivers/drivers.component';
import {ClientsComponent} from './clients/clients.component';
import {InvitationsComponent} from './invitations/invitations.component';
import {DriverFormComponent} from './driver-form/driver-form.component';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    CarFormComponent,
    ModalEditDriverComponent,
    VehiclesComponent,
    DriversComponent,
    ClientsComponent,
    InvitationsComponent,
    DriverFormComponent
  ],
  exports: [],
  providers: [VehicleService, DriverService, EventService, GmapsService]
})
export class AdminModule {
}
