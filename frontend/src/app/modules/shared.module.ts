import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterComponent} from "../components/register/register.component";
import {CustomMaterialModule} from "../core.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {EventComponent} from "../components/event/event.component";
import {BackRouteButtonComponent} from "./shared/back-route-button/back-route-button.component";
import {GoogleMapComponent} from './shared/google-map/google-map.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {TimepickerComponent} from "./shared/timepicker/timepicker.component";
import {TableComponent} from '../components/table/table.component';
import {CalendarComponent} from "../components/calendar/calendar.component";
import {CalendarDayComponent} from "../components/calendar-day/calendar-day.component";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {HttpClient} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TransportEventComponent} from "../components/transport-event/transport-event.component";
import { environment } from 'src/environments/environment';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, environment.translateUrl, '.json');
}

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: false
    }),
    NgxMaterialTimepickerModule.forRoot(),
  ],
  declarations: [
    RegisterComponent,
    EventComponent,
    BackRouteButtonComponent,
    TimepickerComponent,
    TableComponent,
    CalendarComponent,
    CalendarDayComponent,
    GoogleMapComponent,
    TransportEventComponent
  ],
  exports: [
    RegisterComponent,
    EventComponent,
    BackRouteButtonComponent,
    TimepickerComponent,
    TableComponent,
    CalendarComponent,
    CalendarDayComponent,
    TranslateModule,
    GoogleMapComponent,
    TransportEventComponent
  ],
  entryComponents: [TransportEventComponent]
})
export class SharedModule {
}
