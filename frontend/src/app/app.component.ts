import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {fadeAnimation} from "./utils/animations";
import {SidenavService} from "./services/sidenav.service";
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]

})
export class AppComponent implements OnInit {
  isExpanded = false;
  userLang = navigator.language;

  constructor(private translate: TranslateService,
              private sidenavService: SidenavService) {
    // translate.addLangs(['de', 'en', 'pl']);
    translate.addLangs(['en', 'pl']);
    translate.setDefaultLang('en');
    this.setLang().subscribe(() => {
      this.translate.set('assetsPath', environment.assetsUrl);
    });
  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  ngOnInit() {
    this.sidenavService.clickToggleObservable.subscribe((state) => {
      this.isExpanded = state;
    });
  }


  setLang() {
    return this.userLang === 'pl-PL' ? this.translate.use('pl') : this.translate.use('en')
  }
}
