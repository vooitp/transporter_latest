import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailValidator } from '../../utils/customValidators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DriverService } from '../../services/driver.service';
import { SnackBarNotificaionService } from '../../services/snack-bar-notificaion.service';
import { ClientService } from '../../services/client.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: [ './register.component.scss' ]
})
export class RegisterComponent implements OnInit {
	private token: string;
	private email: string;
	public id: number;
	public form: FormGroup;
	public submitted = false;

	public driverData;
	public editMode = false;
	public header: string;
	public registrationMode: boolean;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private clientService: ClientService,
		private driverService: DriverService,
		private snackBarNotificaionService: SnackBarNotificaionService
	) {}

	ngOnInit() {
		this.initForm();
		this.route.params.subscribe((params) => {
			if (params['token'] && params['email']) {
				this.token = params['token'];
				this.email = params['email'];
				this.form.get('email').setValue(this.email);
				this.registrationMode = true;
				this.header = 'Complete registration';
			}
			if (params['id']) {
				this.id = params['id'];
				this.driverService.getDriverById(this.id).subscribe((res) => {
					this.driverData = res;
					this.header = 'Driver details';
					this.parseData(this.driverData, this.form);
					this.handleControlsAbility(this.form, this.editMode);
					this.form.get('password').setValue('');
					this.form.get('retypePassword').setValue('');
				});
			}
		});
	}

	parseData(data, formGroup) {
		let formKeys = Object.keys(formGroup.getRawValue());
		for (let key in formKeys) {
			formGroup.get(formKeys[key]).setValue(data[formKeys[key]]);
		}
	}

	handleControlsAbility(formGroup, editMode) {
		if (!editMode) {
			Object.keys(formGroup.controls).forEach((key) => {
				formGroup.controls[key].disable();
			});
		} else {
			Object.keys(formGroup.controls).forEach((key) => {
				formGroup.controls[key].enable();
			});
		}
	}

	toggleEditMode() {
		this.editMode = !this.editMode;
		if (!this.editMode) {
			Object.keys(this.form.controls).forEach((key) => {
				this.form.controls[key].disable();
			});
		} else {
			Object.keys(this.form.controls).forEach((key) => {
				this.form.controls[key].enable();
			});
		}
	}

	initForm() {
		this.form = new FormGroup({
			firstName: new FormControl('', Validators.required),
			lastName: new FormControl('', Validators.required),
			email: new FormControl('', [ Validators.required, EmailValidator ]),
			phoneNumber: new FormControl('', Validators.required),
			status: new FormControl('', Validators.required),
			password: new FormControl('', Validators.required),
			retypePassword: new FormControl('', Validators.required)
		});
	}

	getForm() {
		let form = this.form.getRawValue();
		delete form.retypePassword;
		return form;
	}

	submitForm(): void {
		this.submitted = true;
		if (this.form.valid) {
			if (this.router.url.includes('client')) {
				this.clientService.registerNewClient(this.getForm(), this.token).subscribe((res) => {
					this.snackBarNotificaionService.openSnackBar('Now you can login');
					setTimeout(() => {
						this.router.navigateByUrl('/login');
					}, 3000);
				});
			}
			if (this.router.url.includes('driver')) {
				this.driverService.registerNewDriver(this.getForm(), this.token).subscribe((res) => {
					this.snackBarNotificaionService.openSnackBar('Now you can login');
					setTimeout(() => {
						this.router.navigateByUrl('/login');
					}, 3000);
				});
			}
		} else {
			this.snackBarNotificaionService.openSnackBar('Fill proper credentials');
		}
	}

	updateForm(): void {
		this.submitted = true;
		if (this.form.valid) {
			this.driverService.updateDriverById(this.id, this.getForm()).subscribe((res) => {
				this.snackBarNotificaionService.openSnackBar(
					`User ${this.getForm().firstName} ${this.getForm().lastName} updated successfully`
				);
				setTimeout(() => {
					this.router.navigateByUrl('/admin');
				}, 3000);
			});
		} else {
			this.snackBarNotificaionService.openSnackBar('Fill proper credentials');
		}
	}
}
