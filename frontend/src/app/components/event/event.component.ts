import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {forkJoin} from "rxjs/observable/forkJoin";

import {milliseconds} from '../../utils/date';
import {DriverService} from "../../services/driver.service";
import {ClientService} from "../../services/client.service";
import {Driver} from "../../interfaces/Driver";
import {VehicleService} from "../../services/vehicle.service";
import {Vehicle} from "../../interfaces/Vehicle";
import {Subscription} from "rxjs/Rx";
import { TransportEventStatus, TransportEventStatusColor } from 'src/app/utils/transport-event-status';
import { ActivatedRoute } from '@angular/router';
import { TransportEventsService } from 'src/app/services/transport-events.service';
import { SnackBarNotificaionService } from 'src/app/services/snack-bar-notificaion.service';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  public eventForm: FormGroup;
  private today = new Date();
  public dateStart;
  public dateEnd;
  public drivers: Driver[] = null;
  public clients = null;
  public vehicles: Vehicle[] = null;
  public TransportEventStatusColor = TransportEventStatusColor;
  public bgSelectColor: string;

  private transportEventId = null;

  constructor(private driverService: DriverService,
              private clientService: ClientService,
              private vehicleService: VehicleService,
              private transportEventsService: TransportEventsService,
              private route: ActivatedRoute,
              private snackBarNotificaionService: SnackBarNotificaionService) {
  }

  get statuses() {
    return Object.keys(TransportEventStatus).map(key => TransportEventStatus[key]);
  }

  ngOnInit() {
    this.initForm();
    this.eventForm.get('dateStart').setValue(this.today);
    this.eventForm.get('dateEnd').setValue(this.today);

    let getDrivers = this.driverService.getDrivers();
    let getClients = this.clientService.getClients();
    let getVehicles = this.vehicleService.getVehicles();

    this.subscription = forkJoin([getDrivers, getClients, getVehicles]).subscribe(results => {
      this.drivers = results[0];
      this.clients = results[1];
      this.vehicles = results[2];
    });

    this.route.params.subscribe((params) => {
      if(params && params.date && !isNaN(Date.parse(params.date))) {
        const dateStart = new Date(params.date);
        const dateEnd = new Date(params.date);
        dateStart.setHours(13, 0, 0, 0);
        dateEnd.setHours(14, 0, 0, 0);

        this.eventForm.get('dateStart').setValue(dateStart);
        this.eventForm.get('dateEnd').setValue(dateEnd);
      } else if(params && params.id) {
        this.transportEventId = params.id;
        this.transportEventsService.getById(params.id).subscribe((data: any) => {
          data.drivers = data.drivers.map(driver => driver._id);
          data.clients = data.clients.map(client => client._id);
          data.vehicle = data.vehicle._id;
          this.eventForm.patchValue(data);
        });
      }

      if(!params || !params.id) {
        navigator.geolocation.getCurrentPosition(position => {
            this.eventForm.get('locationFromCoordinates').setValue({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
              description: ''
            });

            this.eventForm.get('locationToCoordinates').setValue({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
              description: ''
            });
        });
      }
    })
  }

  initForm() {
    this.eventForm = new FormGroup({
      title: new FormControl('', Validators.required),
      dateStart: new FormControl(new Date(), Validators.required),
      dateEnd: new FormControl(new Date(), Validators.required),
      drivers: new FormControl([], Validators.required),
      clients: new FormControl([], Validators.required),
      vehicle: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      description: new FormControl(''),
      locationFromCoordinates: new FormGroup({
        lat: new FormControl('', Validators.required),
        lng: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required)
      }),
      locationToCoordinates: new FormGroup({
        lat: new FormControl('', Validators.required),
        lng: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required)
      })
    })
  }

  submitForm() {
    const formValue = this.eventForm.getRawValue();
    if(this.transportEventId) {
      this.transportEventsService.edit(formValue, this.transportEventId).subscribe(() => {
        this.snackBarNotificaionService.triggerSnackbar('Event has been editted successfully');
      });
    } else {
      this.transportEventsService.add(formValue).subscribe(() => {
        this.snackBarNotificaionService.triggerSnackbar('Event has been added successfully');
      });
    }
  }

  onLocationFromChange(data: {lat: number, lng: number, description: string}) {
    this.eventForm.get('locationFromCoordinates').patchValue(data);
  }

  onLocationToChange(data: {lat: number, lng: number, description: string}) {
    this.eventForm.get('locationToCoordinates').patchValue(data);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
