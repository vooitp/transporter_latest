import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interfaces/User';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit {
	public hide = true;

	constructor(private router: Router, private authService: AuthService) {}

	loginForm: FormGroup;
	submitted = false;

	ngOnInit() {
		this.loginForm = new FormGroup({
			email: new FormControl('', Validators.required),
			password: new FormControl('', Validators.required)
		});
	}

	get formControls() {
		return this.loginForm.controls;
	}

	getFormData() {
		let rawForm: User = this.loginForm.getRawValue();
		return rawForm;
	}

	onSubmit() {
		this.submitted = true;
		if (this.loginForm.valid) {
			this.authService.login(this.getFormData()).subscribe((user) => {
				this.router.navigateByUrl(`/${user.role}`);
			});
		}
	}
}
