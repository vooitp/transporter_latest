import {Component, Inject, AfterViewInit, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {TransportEvent} from 'src/app/interfaces/TransportEvent';
import {checkIfDatesSameDay, weekDays, months} from 'src/app/utils/date';
import {DatePipe} from '@angular/common';
import {AuthService} from 'src/app/services/auth.service';
import {Router} from '@angular/router';
import {ModalConfirmationComponent} from 'src/app/modules/shared/modal-confirmation/modal-confirmation.component';
import {TransportEventsService} from 'src/app/services/transport-events.service';
import {SnackBarNotificaionService} from 'src/app/services/snack-bar-notificaion.service';
import {TranslateService} from "@ngx-translate/core";
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-transport-event',
  templateUrl: './transport-event.component.html',
  styleUrls: ['./transport-event.component.scss'],
  providers: [DatePipe]
})
export class TransportEventComponent implements OnInit{
  public transportEvent: TransportEvent = null;
  public url = environment.url;
  public coordinatesFrom;
  public coordinatesTo;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              public dialogRef: MatDialogRef<TransportEventComponent>,
              public dialog: MatDialog,
              private datePipe: DatePipe,
              private authService: AuthService,
              private router: Router,
              private snackBarNotificaionService: SnackBarNotificaionService,
              private transportEventService: TransportEventsService,
              private translate: TranslateService) {
    this.transportEvent = this.data;
  }

  ngOnInit() {
    this.coordinatesFrom = this.transportEvent.locationFromCoordinates
    this.coordinatesTo = this.transportEvent.locationToCoordinates
  }

  get userRole() {
    return this.authService.getUserRole();
  }

  get formattedDate() {
    if (checkIfDatesSameDay(this.transportEvent.dateStart, this.transportEvent.dateEnd)) {
      const date = this.transportEvent.dateStart;
      const weekDay = weekDays[date.getDay()];
      const day = date.getDate();
      const month = months[date.getMonth()];
      const year = date.getFullYear();
      const timeFrom = this.datePipe.transform(date, 'HH:mm');
      const timeTo = this.datePipe.transform(this.transportEvent.dateEnd, 'HH:mm');
      return `${weekDay}, ${day} of ${month} ${year}, ${timeFrom} - ${timeTo}`;
    } else {
      const dateStart = this.transportEvent.dateStart;
      const dateEnd = this.transportEvent.dateEnd;
      const weekDayStart = weekDays[dateStart.getDay()];
      const weekDayEnd = weekDays[dateEnd.getDay()];
      const dayStart = dateStart.getDate();
      const dayEnd = dateEnd.getDate();
      const monthStart = months[dateStart.getMonth()];
      const monthEnd = months[dateEnd.getMonth()];
      const yearStart = dateStart.getFullYear();
      const yearEnd = dateEnd.getFullYear();
      const timeFrom = this.datePipe.transform(dateStart, 'HH:mm');
      const timeTo = this.datePipe.transform(dateEnd, 'HH:mm');
      return `${weekDayStart}, ${dayStart} of ${monthStart} ${yearStart}, ${timeFrom} - ${weekDayEnd}, ${dayEnd} of ${monthEnd} ${yearEnd}, ${timeTo}`;
    }
  }

  closeTransportEvent() {
    this.dialogRef.close();

  }

  editTransportEvent() {
    this.dialogRef.close();
    this.router.navigateByUrl(`/admin/calendar/event/${this.transportEvent._id}`);
  }
  onLocationFromChange(data: {lat: number, lng: number, description: string}) {
    // this.eventForm.get('locationFromCoordinates').patchValue(data);
  }

  async removeTransportEvent() {
    let payload = await this.translate.get('Notifications.delete_event_confirmation').toPromise();
    // const payload = {title: `Are you sure you want to delete transport event:\n"${this.transportEvent.title}"`};

    const dialogRef = this.dialog.open(ModalConfirmationComponent, {
      width: '300px',
      data: {title: `${payload}:\n"${this.transportEvent.title}"`}
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.transportEventService.remove(this.transportEvent._id).subscribe(() => {
          this.snackBarNotificaionService.triggerSnackbar(`${this.transportEvent.title} Transport event has been removed succesfully`);
          this.dialogRef.close('delete');
        });
      }
    });
  }
}
