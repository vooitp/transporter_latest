import {Component, Input, Output, EventEmitter} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';

interface TableAction {
  id: string;
  icon: string;
  color: string;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent {
  @Input() columns: string[] = [];
  @Input() columnsMap: any = {};
  @Input() actions: TableAction[] = [];
  @Input() dataSource: MatTableDataSource<any>;
  @Input() title: string;
  @Input() addItemTitle: string;
  @Input() addItemLink: string = null;

  @Output() action = new EventEmitter();

  constructor(private router: Router) {
  }

  emitAction(id: string, data?: any) {
    this.action.emit({id, data});
  }

  getTypeOfColumn(column) {
    return typeof this.columnsMap[column];
  }

  redirect() {
    if (this.addItemLink) {
      this.router.navigateByUrl(this.addItemLink);
    } else {
      this.emitAction('add');
    }
  }
}
