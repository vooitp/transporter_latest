import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { TransportEvent } from 'src/app/interfaces/TransportEvent';
import { checkIfDatesSameDay, weekDaysShort } from 'src/app/utils/date';
import { MatDialog } from '@angular/material';
import { TransportEventComponent } from '../transport-event/transport-event.component';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { TransportEventStatusColor } from 'src/app/utils/transport-event-status';

@Component({
	selector: 'app-calendar-day',
	templateUrl: './calendar-day.component.html',
	styleUrls: [ './calendar-day.component.scss' ],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [DatePipe]
})
export class CalendarDayComponent {
	@Input() transportEvents: TransportEvent[] = [];
	@Input() currentMonth: number;
	@Input() view = 'month';
	@Input() date: Date;
	@Input() maxNumberOfEvents: number;

	@Output() switchView = new EventEmitter();
	@Output() deleteEvent = new EventEmitter();

	public TransportEventStatusColor = TransportEventStatusColor;

	constructor(
		public dialog: MatDialog,
		private authService: AuthService,
		private router: Router,
		private datePipe: DatePipe,
		private cd: ChangeDetectorRef
	) {}

	get noTimeDate() {
		return this.datePipe.transform(this.date, 'y-MM-dd')
	}

	get dayNumber() {
		return this.date.getDate();
	}

	get notCurrentMonth() {
		return this.date.getMonth() !== this.currentMonth;
	}

	get today() {
		return checkIfDatesSameDay(this.date, new Date());
	}

	get showOnlyStatus() {
		return this.view === 'month' && this.maxNumberOfEvents === 0;
	}

	get moreEventsLabel() {
		let label = '';
		let moreEvents = 0;
		for(let i = 0; i < this.transportEvents.length; i++) {
			if(this.hideEvent(i, this.transportEvents[i])) {
				moreEvents++;
			}
		}
		if (this.maxNumberOfEvents !== 0 && this.transportEvents.length && moreEvents > 0) {
			label = `${moreEvents}`;
			if (this.maxNumberOfEvents !== 0) {
				label += ' more';
			}
			label += ' events';
		}
		return label;
	}

	get oneDayTransportEvents() {
		return this.transportEvents.filter(event => !event.multiday);
	}

	trackTransportEvent(index: number, transportEvent: TransportEvent) {
		return transportEvent._id;
	}

	isLastMultidayEvent(event: TransportEvent) {
		return (
			event.multiday &&
			event.multidayMainRef &&
			checkIfDatesSameDay(event.dateStart, event.dateEnd)
		);
	}

	getStyle(index: number, event: TransportEvent) {
		return {
			top: this.getEventTopPosition(event), 
			height: this.getEventHeight(event),
			...this.getEventWidthAndLeft(index, event)
		};
	}

	getEventTopPosition(event: TransportEvent) {
		const minutes = event.dateStart.getHours() * 60 + event.dateStart.getMinutes();
		return 50 / 60 * minutes + 'px';
	}

	getEventHeight(event: TransportEvent) {
		const minutes =
			event.dateEnd.getHours() * 60 +
			event.dateEnd.getMinutes() -
			(event.dateStart.getHours() * 60 + event.dateStart.getMinutes());
		return 50 / 60 * minutes + 'px';
	}

	getEventWeekDay(event: TransportEvent) {
		return weekDaysShort[event.dateStart.getDay()];
	} 

	hideEvent(index: number, event: TransportEvent) {
		return (
			(this.maxNumberOfEvents !== 0 &&
			this.maxNumberOfEvents < index + 1 &&
			this.maxNumberOfEvents + 1 !== this.transportEvents.length) ||
			(event.multidayMainRef 
			&& event.multidayMainRef.position >= this.maxNumberOfEvents)
		);
	}

	getEventWidthAndLeft(index: number, event: TransportEvent) {
		let width = 1;
		let left = 0;
		let leftValueFixed = false;
		if (!this.oneDayTransportEvents) {
			return { width: '100%', left: '0' };
		}
		for (const eachEvent of this.oneDayTransportEvents) {
			if (eachEvent === event) {
				leftValueFixed = true;
			} else if (
				eachEvent !== event &&
				((event.dateStart <= eachEvent.dateStart && event.dateEnd >= eachEvent.dateStart) ||
				(event.dateStart >= eachEvent.dateStart && event.dateStart <= eachEvent.dateEnd) ||
				(event.dateEnd >= eachEvent.dateStart && event.dateEnd <= eachEvent.dateEnd))
			) {
				width++;
				if (!leftValueFixed) {
					left++;
				}
			}
		}
		let widthProp = `calc(100% - (100% / ${width} * ${left}))`;
		if(this.oneDayTransportEvents.length === 1) {
			widthProp = '100%';
		}
		return { width: widthProp, left: `calc(100% / ${width} * ${left})`, zIndex: index };
	}

	showTransportEvent(transportEvent: TransportEvent) {
		if (!this.showOnlyStatus) {
			let data = transportEvent;
			if(transportEvent.multidayMainRef) {
				data = transportEvent.multidayMainRef;
			}
			const dialogRef = this.dialog.open(TransportEventComponent, {
				data,
				width: '800px'
			});
			dialogRef.afterClosed().subscribe(result => {
				if(result === 'delete') {
					this.deleteEvent.emit(transportEvent._id);
				}
			});
		} else {
			this.switchToDayView();
		}
	}

	switchToDayView() {
		this.switchView.emit({ view: 'day', date: this.date });
	}

	addNewEvent(event: Event) {
		const target = <HTMLElement> event.target;
		if(target.tagName.toLowerCase() === 'app-calendar-day' || target.classList.contains('calendar-day')) {
			const userRole = this.authService.getUserRole();
			this.router.navigate([`${userRole}/calendar/new-event/${this.date.toISOString()}`]);
		}
	}

	switchToDayViewOnMonth() {
		if(this.view === 'month' && this.showOnlyStatus) {
			this.switchToDayView();
		}
	}
}
