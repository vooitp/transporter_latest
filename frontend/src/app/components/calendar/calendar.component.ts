import {
	Component,
	ChangeDetectionStrategy,
	ElementRef,
	AfterViewInit,
	ChangeDetectorRef,
	ViewChild
} from '@angular/core';
import { TransportEventsService } from 'src/app/services/transport-events.service';
import { Observable } from 'rxjs';
import { TransportEvent } from 'src/app/interfaces/TransportEvent';
import { checkIfDatesSameDay, months, weekDaysShort, getDayDifference } from 'src/app/utils/date';
import { SwipeService } from 'src/app/services/swipe.service';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialog } from '@angular/material';
import { TransportEventComponent } from '../transport-event/transport-event.component';
import { TransportEventStatusColor } from 'src/app/utils/transport-event-status';

@Component({
	selector: 'app-calendar',
	templateUrl: './calendar.component.html',
	styleUrls: [ './calendar.component.scss' ],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarComponent implements AfterViewInit {
	@ViewChild('monthView') monthView;
	@ViewChild('weekView') weekView;
	@ViewChild('dayView') dayView;viewSelect;
	@ViewChild('scheduleView') scheduleView;

	private scheduleEventsLimit = 30;

	private loadingTimeout = null;

	public transportEvents: TransportEvent[] = [];
	public fromDate: Date = null;
	public toDate: Date = null;
	public months = months;
	public weekDaysMap = weekDaysShort;

	public maxScheduleEventsReached = false;
	public minScheduleEventsReached = false;

	public TransportEventStatusColor = TransportEventStatusColor;

	public currentDate = new Date();

	public view = 'week';

	public views = [
		{ value: 'schedule', label: 'Schedule' },
		{ value: 'day', label: 'Day' },
		{ value: 'week', label: 'Week' },
		{ value: 'month', label: 'Month' }
	];

	public hours = [];

	public maxNumberOfEvents = null;

	public today = new Date();

	public eventsLoading = false;

	constructor(
		private transportEventsService: TransportEventsService,
		private elementRef: ElementRef,
		private cd: ChangeDetectorRef,
		private swipeService: SwipeService,
		private authService: AuthService,
		public dialog: MatDialog,
	) {
		switch(this.authService.getUserRole()) {
			case 'admin': {
				this.view = 'month';
				break;
			}
			case 'driver':
			case 'client': {
				this.view = 'schedule';
				break;
			}
		}

		this.fetchTransportEvents().then(() => {
			if(this.view === 'schedule' && this.transportEvents.length < 20) {
				this.fetchTransportEvents();
			}
		})

		for (let i = 0; i < 24; i++) {
			const hour = i > 9 ? i : '0' + i;
			this.hours.push(`${hour}:00`);
		}
	}

	transportEventGroupedByDate(index: number, data: {date: Date, events: TransportEvent[]}) {
		return data.date;
	}

	trackByWeek(index, week) {
		return week['id'];
	}

	trackByDay(index, date: Date) {
		return date.toString();
	}

	trackTransportEvent(index: number, transportEvent: TransportEvent) {
		return transportEvent._id;
	}

	ngAfterViewInit() {
		this.calculateMaxNumOfEvents(true);
		window.addEventListener('resize', () => {
			this.calculateMaxNumOfEvents();
		});

		Observable.merge(
			this.swipeService.detectSwipe(this.monthView.nativeElement, [ 'left', 'right' ]),
			this.swipeService.detectSwipe(this.weekView.nativeElement, [ 'left', 'right' ]),
			this.swipeService.detectSwipe(this.dayView.nativeElement, [ 'left', 'right' ])
		).subscribe((direction: string) => {
			if (direction === 'left') {
				this.switchNext();
			} else {
				this.switchPrev();
			}
			this.cd.markForCheck();
		});
	}

	get currentMonth() {
		return this.currentDate.getMonth();
	}

	get currentYear() {
		return this.currentDate.getFullYear();
	}

	get weeks() {
		const days = [];
		const lastDayOfCurrentMonth = new Date(this.currentYear, this.currentMonth + 1, 0);
		const firstDayOfCurrentMonth = new Date(this.currentYear, this.currentMonth, 1);
		const lastDayOfPrevMonth = new Date(this.currentYear, this.currentMonth, 0);

		if (firstDayOfCurrentMonth.getDay()) {
			for (let i = 0; i < firstDayOfCurrentMonth.getDay() - 1; i++) {
				days.splice(0, 0, new Date(this.currentYear, this.currentMonth - 1, lastDayOfPrevMonth.getDate() - i));
			}
		}

		for (let i = 0; i < lastDayOfCurrentMonth.getDate(); i++) {
			days.push(new Date(this.currentYear, this.currentMonth, i + 1));
		}

		let moreDaysOffset = 0;
		if (lastDayOfCurrentMonth.getDay()) {
			moreDaysOffset = 7 - lastDayOfCurrentMonth.getDay();
			for (let i = 0; i < 7 - lastDayOfCurrentMonth.getDay(); i++) {
				days.push(new Date(this.currentYear, this.currentMonth + 1, i + 1));
			}
		}

		const moreDays = 7 * (6 - days.length / 7);

		for (let i = 0; i < moreDays; i++) {
			days.push(new Date(this.currentYear, this.currentMonth + 1, moreDaysOffset + i + 1));
		}

		const weeks = [];
		for (let i = 0; i < days.length / 7; i++) {
			weeks.push(days.slice(7 * i, 7 * i + 7));
			weeks[i]['id'] = `week-${days[i*7].toString()}`;
		}
		return weeks;
	}

	get week() {
		const days: Date[] = [];
		const currentDay = this.currentDate.getDay() ? this.currentDate.getDay() - 1 : 6;

		for (let i = 0; i < currentDay; i++) {
			days.splice(0, 0, new Date(this.currentYear, this.currentMonth, this.currentDate.getDate() - 1 - i));
		}

		days.push(this.currentDate);

		for (let i = 0; i < 6 - currentDay; i++) {
			days.push(new Date(this.currentYear, this.currentMonth, this.currentDate.getDate() + 1 + i));
		}

		return days;
	}

	get weekMonth() {
		const firstMonth = this.week[0].getMonth();
		const lastMonth = this.week[this.week.length - 1].getMonth();
		if (firstMonth !== lastMonth) {
			return `${this.months[firstMonth]} - ${this.months[lastMonth]}`;
		}
		return this.months[firstMonth];
	}

	get transportEventsGroupedByDate(): Array<{date: Date, events: TransportEvent[]}> {
		return this.transportEvents.reduce((arr, event) => {
			const item = arr.find(item => checkIfDatesSameDay(item.date, event.dateStart));
			if(item) {
				item.events.push(event);
			} else {
				arr.push({
					date: event.dateStart,
					events: [event]
				});
			}
			return arr;
		}, []).sort((a, b) => {
			if(a.date < b.date) {
				return -1;
			} else if(a.date > b.date) {
				return 1;
			} else {
				return 0;
			}
		});
	}

	fetchTransportEvents(further: boolean = false) {
		return new Promise(resolve => {
			switch(this.view) {
				case 'month':
				case 'week':
				case 'day': {
					let fromDate = null;
					let toDate = null;
					if(!this.fromDate && !this.toDate) {
						this.fromDate = new Date(this.currentDate);
						this.fromDate.setMonth(this.fromDate.getMonth() - 1);
						this.fromDate.setDate(1);
						this.fromDate.setHours(0, 0, 0);

						this.toDate = new Date(this.currentDate);
						this.toDate.setMonth(this.toDate.getMonth() + 2);
						this.toDate.setDate(0);
						this.toDate.setHours(0, 0, 0);

						fromDate = this.fromDate;
						toDate = this.toDate;
					} else {
						if(this.fromDate.getMonth() === this.currentDate.getMonth()) {
							fromDate = this.fromDate = new Date(this.fromDate.getFullYear(), this.fromDate.getMonth()-1, 1);
							toDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);
						} else if(this.toDate.getMonth() === this.currentDate.getMonth()) {
							toDate = this.toDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth()+2, 0);
							fromDate = new Date(toDate.getFullYear(), toDate.getMonth(), 1);
						}
					}

					if(fromDate && toDate) {
						this.startLoadingTimeout();
						this.transportEventsService.get(fromDate, toDate).map(events => {
							return events.filter(event => !this.transportEvents.find(transportEvent => transportEvent._id === event._id));
						}).subscribe(events => {
							this.addTransportEvents(events);
							this.stopLoadingTimeout();
							this.cd.markForCheck();
						}, null, resolve);
					} else {
						resolve();
					}
				}
				case 'schedule': {
					if(this.eventsLoading) {
						return resolve();
					}
					this.startLoadingTimeout();
					if(!this.fromDate && !this.toDate) {
						this.fromDate = new Date(this.currentDate);
						this.transportEventsService.get(this.fromDate, null, this.scheduleEventsLimit).map(events => {
							return events.filter(event => !this.transportEvents.find(transportEvent => transportEvent._id === event._id));
						}).subscribe(events => {
							this.addTransportEvents(events);
							this.stopLoadingTimeout();
							this.cd.markForCheck();
						}, null, resolve);
					} else if(further && !this.maxScheduleEventsReached) {
						this.transportEventsService.get(this.toDate, null, this.scheduleEventsLimit).map(events => {
							return events.filter(event => !this.transportEvents.find(transportEvent => transportEvent._id === event._id));
						}).subscribe(events => {
							if(!events.length) {
								this.maxScheduleEventsReached = true;
							} else {
								this.addTransportEvents(events);
							}
							this.stopLoadingTimeout();
							this.cd.markForCheck();
						}, null, resolve);
					} else if(!further && !this.minScheduleEventsReached) {
						const container = <HTMLElement> this.scheduleView.nativeElement;
						const scrollHeight = container.scrollHeight;
						this.transportEventsService.get(null, this.fromDate, this.scheduleEventsLimit).map(events => {
							return events.filter(event => !this.transportEvents.find(transportEvent => transportEvent._id === event._id));
						}).subscribe(events => {
							if(!events.length) {
								this.minScheduleEventsReached = true;
							} else {
								this.addTransportEvents(events);
							}
							this.stopLoadingTimeout();
							this.cd.markForCheck();
							setTimeout(() => {
								container.scrollTop = container.scrollHeight - scrollHeight;
							}, 0);
						}, null, resolve);
					} else {
						this.stopLoadingTimeout();
						resolve();
					}
				}
			}
		});
	}

	startLoadingTimeout() {
		this.eventsLoading = true;
		console.log('show');
		this.cd.markForCheck();
	}

	stopLoadingTimeout() {
		clearTimeout(this.loadingTimeout);
		this.loadingTimeout = null;
		this.eventsLoading = false;
		console.log('hide');
	}

	addTransportEvents(events: TransportEvent[]) {
		this.transportEvents = this.transportEvents.concat(events);
		for(const event of events) {
			if(!this.fromDate || event.dateStart < this.fromDate) {
				this.fromDate = event.dateStart;
			}
			if(!this.toDate || event.dateEnd > this.toDate) {
				this.toDate = event.dateEnd;
			}
		}

		for(const item of this.transportEventsGroupedByDate) {
			const multiDayEvents: TransportEvent[] = [];
			const multiDayMainEvents: TransportEvent[] = [];

			for(const event of item.events) {
				if(event.multiday && event.multidayMainRef) {
					multiDayEvents.push(event);
				} else if(event.multiday && !event.multidayMainRef) {
					multiDayMainEvents.push(event);
				}
			}

			if(!multiDayEvents.length && multiDayMainEvents.length) {
				multiDayMainEvents.map((event, i) => {
					event.position = i;
				});
			}
		}
	}

	getTransportEvents(day: Date) {
		let events = this.transportEvents
			.filter((event) => checkIfDatesSameDay(event.dateStart, day))
			.sort((a, b) => (a.dateStart < b.dateStart ? -1 : (a.dateStart > b.dateStart ? 1 : 0)));
		const multiDayEvents = [];
		const multiDayMainEvents = [];
		const singleDayEvents = [];
		for(const event of events) {
			if(event.multiday && event.multidayMainRef) {
				multiDayEvents.push(event);
			} else if(event.multiday && !event.multidayMainRef) {
				multiDayMainEvents.push(event);
			} else {
				singleDayEvents.push(event);
			}
		}
		if(!multiDayEvents.length && multiDayMainEvents.length) {
			events = [
				...multiDayMainEvents,
				...singleDayEvents
			];
		} else if(multiDayEvents.length) {
			multiDayEvents.sort((a, b) => (a.dateStart < b.dateStart ? -1 : (a.dateStart > b.dateStart ? 1 : 0)));
			const maxPosition = Math.max(...multiDayEvents.map(event => event.multidayMainRef.position));
			const occupiedPositions = multiDayEvents.map(event => event.multidayMainRef.position);

			const otherEvents = [
				...multiDayMainEvents,
				...singleDayEvents
			];

			events = [];
			for(let i = 0; i <= maxPosition; i++) {
				if(!occupiedPositions.includes(i) && otherEvents.length) {
					const event = otherEvents.shift();
					event.position = i;
					events.push(event);
				} else if(occupiedPositions.includes(i)) {
					const event = multiDayEvents.shift();
					event.position = i;
					if(!otherEvents.length) {
						const lastPosition = events.length ? events[events.length - 1].position : -1;
						event.offsetTop = event.position - lastPosition - 1;
					}
					events.push(event);
				}
			}
			for(const event of otherEvents) {
				const lastPosition = events.length ? events[events.length - 1].position : -1;
				event.position = lastPosition + 1;
				events.push(event);
			}

			for(const event of multiDayEvents) {
				const lastPosition = events.length ? events[events.length - 1].position : -1;
				event.position = event.multidayMainRef.position;
				event.offsetTop = event.position - lastPosition - 1;
				events.push(event);
			}
		}
		return events;
	}

	getAllDayTransportEvents(day: Date) {
		const startWeekDate = new Date(day);
		startWeekDate.setHours(0, 0, 0);

		const endWeekDate = new Date(day);
		endWeekDate.setHours(23, 59, 59);

		if(this.view === 'week') {
			if(startWeekDate.getDay() !== 1) {
				startWeekDate.setDate(startWeekDate.getDate() - startWeekDate.getDay() + 1);
			}

			if(endWeekDate.getDay() !== 0) {
				endWeekDate.setDate(endWeekDate.getDate() + (6 - endWeekDate.getDay()) + 1);
			}
		}
		return this.transportEvents
			.filter(event => {
				return (
					((event.dateStart <= startWeekDate && event.dateEnd >= startWeekDate) ||
					(event.dateStart >= startWeekDate && event.dateStart <= endWeekDate) ||
					(event.dateEnd >= startWeekDate && event.dateEnd <= endWeekDate)) &&
					event.multiday && !event.multidayMainRef
				);
			})
			.sort((a, b) => (a.dateStart < b.dateStart ? -1 : (a.dateStart > b.dateStart ? 1 : 0)));
	}

	getAllDayTransportEventStyle(event: TransportEvent) {
		const dateStart = new Date(event.dateStart);
		dateStart.setHours(0, 0, 0);

		const dateEnd = new Date(event.dateEnd);
		dateEnd.setHours(0, 0, 0);

		const startWeekDate = new Date(event.dateStart);
		startWeekDate.setHours(0, 0, 0);

		if(this.view === 'week' && startWeekDate.getDay() !== 1) {
			startWeekDate.setDate(startWeekDate.getDate() - startWeekDate.getDay() + 1);
		}

		const dayDifference = getDayDifference(event.dateStart, event.dateEnd);
		let width = `calc(100% / 7 * ${dayDifference + 1})`;
		if(dayDifference === 0) {
			width = '100%';
		}

		let marginLeft = `calc(100% / 7 * ${(dateStart.getTime() - startWeekDate.getTime()) / (1000 * 60 * 60 * 24)})`;
		if(dateStart.getTime() - startWeekDate.getTime() === 0) {
			marginLeft = '0';
		}

		return {
			width,
			'margin-left': marginLeft
		};
	}

	showTransportEvent(transportEvent: TransportEvent) {
		const dialogRef = this.dialog.open(TransportEventComponent, {
			data: transportEvent,
			width: '800px'
		});


		dialogRef.afterClosed().subscribe(result => {
			if(result === 'delete') {
				this.removeEvent(transportEvent._id);
			}
		});
	}

	switchPrev() {
		switch (this.view) {
			case 'month': {
				this.currentDate.setMonth(this.currentDate.getMonth() - 1);
				break;
			}
			case 'week': {
				this.currentDate.setDate(this.currentDate.getDate() - 7);
				break;
			}
			case 'day': {
				this.currentDate.setDate(this.currentDate.getDate() - 1);
				break;
			}
		}

		this.fetchTransportEvents();
	}

	switchNext() {
		switch (this.view) {
			case 'month': {
				this.currentDate.setMonth(this.currentDate.getMonth() + 1);
				break;
			}
			case 'week': {
				this.currentDate.setDate(this.currentDate.getDate() + 7);
				break;
			}
			case 'day': {
				this.currentDate.setDate(this.currentDate.getDate() + 1);
				break;
			}
		}

		this.fetchTransportEvents(true);
	}

	isToday(date: Date) {
		return checkIfDatesSameDay(date, new Date());
	}

	calculateMaxNumOfEvents(noChangeCheck?: boolean) {
		setTimeout(() => {
			if (this.view === 'month') {
				const container = this.elementRef.nativeElement.querySelector('.calendar__month');
				container.offsetHeight;
				const height = container.offsetHeight / 6;
				const maxNumberOfEvents = Math.floor((height - 25 - 20) / 20);
				this.maxNumberOfEvents = maxNumberOfEvents >= 0 ? maxNumberOfEvents : 0;
			} else {
				this.maxNumberOfEvents = null;
			}
			if (!noChangeCheck) {
				this.cd.markForCheck();
			}
		}, 0);
	}

	onSwitchView({ view, date }: { view: string; date: Date }) {
		this.currentDate = date;
		this.view = view;
	}

	onSelectSwitchView() {
		this.fromDate = null;
		this.toDate = null;
		this.calculateMaxNumOfEvents();
		this.fetchTransportEvents().then(() => {
			if(this.view === 'schedule' && this.transportEvents.length < 20) {
				this.fetchTransportEvents();
			}
		})
	}

	checkScheduleEvents(event) {
		const container = <HTMLElement> this.scheduleView.nativeElement;
		if(container.scrollTop === container.scrollHeight - container.offsetHeight) {
			this.fetchTransportEvents(true);
		} else if(container.scrollTop === 0) {
			this.fetchTransportEvents();
		}
	}

	removeEvent(id: string) {
		this.transportEvents = this.transportEvents.filter(event => {
			return (
				event._id !== id &&
				(event._id.indexOf('/') === -1 || event._id.substr(0, event._id.indexOf('/')) !== id)
			);
		});
	}
}
