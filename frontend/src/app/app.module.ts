import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {CustomMaterialModule} from "./core.module";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {routing} from './app-routing.module';
import {AuthInterceptor} from './utils/http-interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {HeaderComponent} from './modules/shared/header/header.component';
import {AuthService} from "./services/auth.service";
import {RoleGuard} from "./guard/RoleGuard";
import {ModalInvitationComponent} from "./modules/admin/modals/modal/modalInvitation.component";
import {ModalConfirmationComponent} from "./modules/shared/modal-confirmation/modal-confirmation.component";
import {ModalAwaitingComponent} from "./modules/admin/modals/modal-awaiting/modal-awaiting.component";
import {LoginComponent} from './components/login/login.component';
import {SideBarComponent} from "./modules/shared/side-bar/side-bar.component";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, environment.translateUrl, '.json');
}


@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('./frontend/ngsw-worker.js', { enabled: environment.production }),
  ],
  declarations: [
    AppComponent,
    SideBarComponent,
    LoginComponent,
    HeaderComponent,
    ModalInvitationComponent,
    ModalAwaitingComponent,
    ModalConfirmationComponent
    ],
  providers: [
    AuthService,
    RoleGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    ModalInvitationComponent,
    ModalAwaitingComponent,
    ModalConfirmationComponent
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}


