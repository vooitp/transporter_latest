export const UserRoles = {
    ADMIN: 'admin',
    DRIVER: 'driver',
    CLIENT: 'client'
};