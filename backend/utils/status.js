export const ClientStatus = {
    PENDING: 'pending',
    ACTIVE: 'active',
    DISABLED: 'disabled'
};

export const DriverStatus = {
    PENDING: 'pending',
    ACTIVE: 'active',
    DISABLED: 'disabled'
};

export const VehicleStatus = {
    ACTIVE: 'active',
    DISABLED: 'disabled'
};

export const TransportEventStatus = {
    PAID_FINISHED: 'paid, finished',
    UNPAID_FINISHED: 'unpaid, finished, invoicedd',
    FINISHED_UNINVOICED: 'finished, uninvoiced',
    FINISHED_SUBTRACTOR_TO_PAY: 'subcontractor finished, paid by cliend, subcontractor is to be paid'
};