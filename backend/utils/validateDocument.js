const validateDocument = document => {
    const err = document.validateSync();

    const errors = [];
    if(err) {
        for(const prop of Object.keys(err.errors)) {
            errors.push({prop, message: err.errors[prop].message});
        }
    }

    return errors;
}

export default validateDocument;