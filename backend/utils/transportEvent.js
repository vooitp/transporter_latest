import { UserRoles } from "./UserRoles";

export const getTransportEventsParams = (role, userId, fromDate, toDate) => {
    let select = '';
    let populate = 'clients drivers vehicle';
    let find = {};
    if(role === UserRoles.DRIVER) {
        select = '-createdAt -modifiedAt -driver';
        find.drivers = { $in : [userId] };
    } else if(role === UserRoles.CLIENT) {
        select = '-createdAt -modifiedAt -client';
        find.clients = { $in : [userId] };
    }

    if(fromDate || toDate) {
        find = { '$and': [find] };

        if(fromDate) {
            find['$and'].push({'dateStart': { $gte: fromDate }});
        }

        if(toDate) {
            find['$and'].push({'dateEnd': { $lte: toDate }});
        }
    }

    return {
        select,
        populate,
        find
    };
}