import nodemailer from 'nodemailer';

const { user, pass } = {
    "user": "the.transporter.pl@gmail.com",
    "pass": "wivdrmfgnhvohtqx" // \SX5J(uqM4sNS@n, 
};

const smtpConfig = {
	service: 'Gmail',
	secure: true, // use SSL
	auth: {
		user,
		pass
	},
	rejectUnauthorized: false,
	tls: {
        rejectUnauthorized: false
    }
};

const transporter = nodemailer.createTransport(smtpConfig);

export const mailer = (email, html, subject) => {
	const mailOptions = {
		from: 'geka764902@gmail.com',
		to: email,
		subject,
		html
	};

	return new Promise((resolve, reject) => {
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) reject(error);
			resolve(info);
		});
	});
};
