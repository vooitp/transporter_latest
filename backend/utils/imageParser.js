import fs from 'fs';
import shortid from 'shortid';
import dataUriToBuffer from 'data-uri-to-buffer';
import {mkdirsSync} from 'mkdir';

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-@');

export default (folder, currentImages, oldImages) => {
    const isSingle = (currentImages && currentImages.constructor !== Array);
    
    let newImages = currentImages || [];
    if(isSingle) {
        newImages = [currentImages];
    }

    let originalImages = oldImages || [];
    if(originalImages.constructor !== Array) {
        originalImages = [oldImages];
    }

    const imagesToDelete = originalImages.filter(originalImage => newImages.indexOf(originalImage) === -1);
    for(const image of imagesToDelete) {
        if(fs.existsSync(`./public${image}`)) {
            fs.unlinkSync(`./public${image}`);
        }
    }

    for(let i=0; i<newImages.length; i++) {
        let image = newImages[i];
        if(image.substr(0, 10) === 'data:image') {
            const extensions = image.match(/jpeg|png|svg/i);
            const extension = extensions ? extensions[0] : 'png';
            const filename = shortid.generate();

            const buffered = dataUriToBuffer(image);

            const path = `${folder}${filename}.${extension}`;
            image = `./public${path}`;
            if (!fs.existsSync(`./public${folder}`)) {
                mkdirsSync(`./public${folder}`);
            }
            fs.writeFileSync(image, buffered, 'binary');
            newImages[i] = path;
        }
    }

    if(isSingle) {
        return newImages[0];
    }
    return newImages;
}