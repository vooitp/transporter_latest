const to = promise => {
	return promise
		.then((data) => {
			if(data.constructor !== Array && typeof data.__v !== 'undefined' && typeof data._id !== 'undefined') {
				return [null, data.toObject()];
			}
			return [null, data];
		})
		.catch((err) => [err]);
};

export default to;