const swaggerJsdoc = require('swagger-jsdoc');

export const swaggerSpecs = swaggerJsdoc({
	swaggerDefinition: {
		info: {
			title: 'Transporter API',
			version: '1.0.0'
        },
        produces: ['application/json'],
        consumes: ['application/json'],
        securityDefinitions: {
            jwt: {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header'
            }
        },
        security: [
            { 
                jwt: [] 
            }
        ]
	},
	apis: [ './backend/routes/api/*.js' ]
});

export const swaggerOptions = {
    swaggerOptions: {
        operationsSorter: (a, b) => {
			const methodsOrder = ["get", "post", "put", "delete"];
            let result = methodsOrder.indexOf(a.get("method")) - methodsOrder.indexOf(b.get("method"));

            if (result === 0) {
                result = a.get("path").localeCompare(b.get("path"));
            }

            return result;
        }
    }
}