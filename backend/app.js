import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';
import routes from './routes/index';
import {swaggerSpecs, swaggerOptions} from './swagger';
import fs from 'fs';

const appDir = path.dirname(require.main.filename);

const swaggerUi = require('swagger-ui-express');

mongoose.Promise = global.Promise;
const app = express();

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(appDir, 'public')));

const port = process.env.PORT || 3001;

mongoose.Promise = global.Promise;

let url = 'mongodb+srv://vooit:qwerty123456@transporter-oah1z.mongodb.net/transporter?retryWrites=true';
if(process.env.NODE_ENV === 'development') {
    url = 'mongodb://localhost:27017/transporter';
}
mongoose.connect(url, { useNewUrlParser: true })
    .then(() => {
        console.log('Connected to database!');
    })
    .catch(() => {
        console.log('Connection failed!');
    });

SourceMapSupport.install();

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs, swaggerOptions));

routes(app);

const frontendHandler = (req, res) => {
    let data = fs.readFileSync(path.join(appDir, 'public/frontend/index.html'), 'utf8');
    res.send(data);
};

app.use('/*', frontendHandler);

app.use((req, res, next) => {
    res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});

app.listen(port, () => {
    console.log(`App Server Listening at ${port}`);
});

export default app;