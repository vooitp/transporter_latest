const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InvitationSchema = new Schema({
    token: {
        type: String,
        required: true
    },
    userRole: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    expirationDate: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Invitation', InvitationSchema);
