const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const VehicleSchema = new Schema({
    make: {
        type: String,
        required: true
    },
    yearOfManufacture: {
        type: String,
        required: true
    },
    photos: [String],
    status: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Vehicle', VehicleSchema);
