const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TransportEventSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	description: String,
	locationFromCoordinates: {
		type: {
			lat: Number,
			lng: Number,
			description: String
		},
		required: true
	},
	locationToCoordinates: {
		type: {
			lat: Number,
			lng: Number,
			description: String
		},
		required: true
	},
	dateStart: {
		type: String,
		required: true
	},
	dateEnd: {
		type: String,
		required: true
	},
	drivers: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Driver',
			required: true
		}
	],
	clients: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Client',
			required: true
		}
	],
	vehicle: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Vehicle',
		required: true
	},
	createdAt: String,
	modifiedAt: String,
	status: {
		type: String,
		required: true
	}
});

module.exports = mongoose.model('TransportEvent', TransportEventSchema);
