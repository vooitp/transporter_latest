import request from 'supertest';

import app from '../app';
import { initData } from './data';

/**
 * User login with email
 */
describe('POST /login', function () {
    let admin = null;
    let client = null;
    let driver = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({admin, client, driver} = data);
            done();
        })
    });

    it('validates admin access with invalid email', function (done) {
        const data = {
            email: 'hacker@gmail.com',
            password: 'testing'
        };
        request(app)
            .post('/api/login')
            .send(data)
            .expect(404, done);
    });

    it('validates admin access with valid email but invalid password', function (done) {
        const data = {
            email: admin.email,
            password: 'sdfsf'
        };
        request(app)
            .post('/api/login')
            .send(data)
            .expect(404, done);
    });

    it('validates admin access with valid email and valid password', function (done) {
        const data = {
            email: admin.email,
            password: '123456'
        };
        request(app)
            .post('/api/login')
            .send(data)
            .expect(200)
            .end((err, res) => {
                if(res.body.role !== 'admin') {
                    return done(new Error('Not admin was signed in'));
                }
                done();
            });
    });

    it('validates driver access with valid email and valid password', function (done) {
        const data = {
            email: driver.email,
            password: '123456'
        };
        request(app)
            .post('/api/login')
            .send(data)
            .expect(200)
            .end((err, res) => {
                if(res.body.role !== 'driver') {
                    return done(new Error('Not driver was signed in'));
                }
                done();
            });
    });

    it('validates client access with valid email and valid password', function (done) {
        const data = {
            email: client.email,
            password: '123456'
        };
        request(app)
            .post('/api/login')
            .send(data)
            .expect(200)
            .end((err, res) => {
                if(res.body.role !== 'client') {
                    return done(new Error('Not client was signed in'));
                }
                done();
            });
    });
});