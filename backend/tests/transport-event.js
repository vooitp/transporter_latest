import request from 'supertest';

import app from '../app';
import TransportEvent from '../models/TransportEvent';
import { initData } from './data';


describe('GET api/transport-event/', function () {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let transportEvent = null;
    let transportEventDriverClient = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, clientToken, transportEvent, transportEventDriverClient} = data);
            done();
        })
    });

    it('get all transport events by driver', function (done) {
        request(app)
            .get('/api/transport-event?fromDate=2019-03-09T00:00:00.000Z&toDate=2019-03-11T00:00:00.000Z')
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Transparent events is not an array'));
                }
                if(res.body.length !== 1) {
                    return done(new Error('There must 1 transport event for driver'));
                }
                if(res.body[0]._id.toString() !== transportEvent._id.toString()) {
                    return done(new Error('Wrong transport event was returned for driver'));
                }
                done();
            });
    });

    it('get all transport events by client', function (done) {
        request(app)
            .get('/api/transport-event?fromDate=2019-03-09T00:00:00.000Z&toDate=2019-03-11T00:00:00.000Z')
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Transparent events is not an array'));
                }
                if(res.body.length !== 1) {
                    return done(new Error('There must 1 transport event for client'));
                }
                if(res.body[0]._id.toString() !== transportEvent._id.toString()) {
                    return done(new Error('Wrong transport event was returned for client'));
                }
                done();
            });
    });

    it('get all transport events by admin', function (done) {
        request(app)
            .get('/api/transport-event?fromDate=2019-03-09T00:00:00.000Z&toDate=2019-03-11T00:00:00.000Z')
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Transparent events is not an array'));
                }
                if(res.body.length !== 2) {
                    return done(new Error('There must 3 transport events for admin'));
                }
                const returnedIds = res.body.map(item => item._id.toString()).sort();
                const properIds = [transportEvent._id.toString(), transportEventDriverClient._id.toString()].sort();
                if(JSON.stringify(returnedIds) !== JSON.stringify(properIds)) {
                    return done(new Error('Wrong transport events were returned for admin'));
                }
                done();
            });
    });
});

describe('GET api/transport-event/{id}', function () {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let transportEvent = null;
    let transportEventDriverClient = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, clientToken, transportEvent, transportEventDriverClient} = data);
            done();
        })
    });

    it('forbid receiving transport event by driver', function (done) {
        request(app)
            .get('/api/transport-event/'+transportEventDriverClient._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(404, done);
    });

    it('forbid receiving transport event by client', function (done) {
        request(app)
            .get('/api/transport-event/'+transportEventDriverClient._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(404, done);
    });

    it('get transport event by driver', function (done) {
        request(app)
            .get('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(transportEvent._id.toString() !== res.body._id.toString()) {
                    return done(new Error('Returned object is not the proper one'));
                }
                done();
            })
    });

    it('get transport event by client', function (done) {
        request(app)
            .get('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(transportEvent._id.toString() !== res.body._id.toString()) {
                    return done(new Error('Returned object is not the proper one'));
                }
                done();
            })
    });

    it('get transport event by admin', function (done) {
        request(app)
            .get('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(transportEvent._id.toString() !== res.body._id.toString()) {
                    return done(new Error('Returned object is not the proper one'));
                }
                done();
            });
    });
});

describe('PUT api/transport-event/{id}', function() {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let transportEvent = null;

    const propToTest = 'title';

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, clientToken, transportEvent} = data);
            done();
        })
    });

    it('edit transport event by driver', function (done) {
        request(app)
            .put('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .send({[propToTest]: transportEvent[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit transport event by client', function (done) {
        request(app)
            .put('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .send({[propToTest]: transportEvent[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit transport event by admin', function(done) {
        request(app)
            .put('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .send({[propToTest]: transportEvent[propToTest]+'Testing'})
            .expect(200)
            .end(err => {
                if(err) throw err;
                TransportEvent.findById(transportEvent._id).then(changedTransportEvent => {
                    for(const prop in transportEvent.toObject()) {
                        if(prop !== propToTest && prop !== 'modifiedAt' && JSON.stringify(changedTransportEvent[prop]) !== JSON.stringify(transportEvent[prop])) {
                            return done(new Error(`property ${prop} has changed unintentionally: from ${transportEvent[prop]} to ${changedTransportEvent[prop]}`));
                        } else if((prop === propToTest || prop === 'modifiedAt') && changedTransportEvent[prop] === transportEvent[prop]) {
                            return done(new Error(`property ${prop} hasn't changed`));
                        } else if(prop === propToTest && changedTransportEvent[prop] !== transportEvent[prop]+'Testing') {
                            return done(new Error(`property ${prop} has change to improper value: from ${transportEvent[prop]} to ${changedTransportEvent[prop]}`));
                        }
                    }
                    done();
                }).catch(err => {
                    console.log(err);
                    done(err);
                })
            });
    });
});

describe('DELETE api/transport-event/{id}', function() {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let transportEvent = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, transportEvent, driverToken, clientToken} = data);
            done();
        })
    });

    it('delete transport event by driver', function (done) {
        request(app)
            .delete('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('delete transport event by client', function (done) {
        request(app)
            .delete('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('delete transport event by admin', function(done) {
        request(app)
            .delete('/api/transport-event/'+transportEvent._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200, done);
    });
});