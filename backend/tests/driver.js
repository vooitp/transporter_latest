import request from 'supertest';

import app from '../app';
import Driver from '../models/Driver';
import validateDocument from '../utils/validateDocument';
import { initData } from './data';


describe('GET api/driver/', function () {
    let adminToken = null;
    let driverToken = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken} = data);
            done();
        })
    });

    it('get all drivers by driver', function (done) {
        request(app)
            .get('/api/driver')
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('get all drivers by admin', function (done) {
        request(app)
            .get('/api/driver')
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Drivers is not an array'));
                }
                for(const driver of res.body) {
                    const driverObj = new Driver(driver);
                    const errors = validateDocument(driverObj);
                    if(errors.length) {
                        return done(new Error(errors));
                    }
                }
                done();
            })
    });
});

describe('GET api/driver/{id}', function () {
    let adminToken = null;
    let driverToken = null;
    let tempDriverToken = null;
    let driver = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, tempDriverToken, driver} = data);
            done();
        })
    });

    it('get driver by another driver', function (done) {
        request(app)
            .get('/api/driver/'+driver._id)
            .set('Authorization', 'Bearer ' + tempDriverToken)
            .expect(401, done)
    });

    it('get driver by the same driver', function (done) {
        request(app)
            .get('/api/driver/'+driver._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(JSON.stringify(driver) !== JSON.stringify(res.body)) {
                    return done(new Error('Returned object is not the proper'));
                }
                done();
            })
    });

    it('get driver by admin', function (done) {
        request(app)
            .get('/api/driver/'+driver._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(JSON.stringify(driver) !== JSON.stringify(res.body)) {
                    return done(new Error('Returned object is not the proper'));
                }
                done();
            })
    });
});

describe('PUT api/driver/{id}', function() {
    let adminToken = null;
    let driverToken = null;
    let tempDriverToken = null;
    let driver = null;
    let tempDriver = null;

    const propToTest = 'lastName';

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, tempDriverToken, driver, tempDriver} = data);
            done();
        })
    });

    it('edit driver by admin', function(done) {
        request(app)
            .put('/api/driver/'+driver._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .send({[propToTest]: driver[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit driver by another driver', function(done) {
        request(app)
            .put('/api/driver/'+tempDriver._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .send({[propToTest]: driver[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit driver by the same driver', function(done) {
        request(app)
            .put('/api/driver/'+tempDriver._id)
            .set('Authorization', 'Bearer ' + tempDriverToken)
            .send({[propToTest]: tempDriver[propToTest]+'Testing'})
            .expect(200)
            .end(err => {
                if(err) throw err;
                Driver.findById(tempDriver._id).then(changedDriver => {
                    for(const prop in tempDriver.toObject()) {
                        if(prop !== propToTest && JSON.stringify(changedDriver[prop]) !== JSON.stringify(tempDriver[prop])) {
                            return done(new Error(`property ${prop} has changed unintentionally`));
                        } else if(prop === propToTest && changedDriver[prop] === tempDriver[prop]) {
                            return done(new Error(`property ${prop} hasn't changed`));
                        } else if(prop === propToTest && changedDriver[prop] !== tempDriver[prop]+'Testing') {
                            return done(new Error(`property ${prop} has change to improper value: from ${tempDriver[prop]} to ${changedDriver[prop]}`));
                        }
                    }
                    done();
                });
            });
    });
});

describe('DELETE api/driver/{id}', function() {
    let driverToken = null;
    let tempDriverToken = null;
    let tempDriver = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({driverToken, tempDriverToken, tempDriver} = data);
            done();
        })
    });

    it('delete driver by another driver', function(done) {
        request(app)
            .delete('/api/driver/'+tempDriver._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('delete driver by the same driver', function(done) {
        request(app)
            .delete('/api/driver/'+tempDriver._id)
            .set('Authorization', 'Bearer ' + tempDriverToken)
            .expect(200, done);
    });
});