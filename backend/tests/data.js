import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';

import Admin from '../models/Admin';
import Driver from '../models/Driver';
import Client from '../models/Client';
import Vehicle from '../models/Vehicle';
import TransportEvent from '../models/TransportEvent';
import { tokenSecret } from '../utils/tokenSecret';
import { DriverStatus, ClientStatus, VehicleStatus } from '../utils/status';

const drivers = [];
const clients = [];

const admin = {
    email: 'admin@gmail.com',
    password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
    firstName: 'Admin',
    lastName: 'admin'
};

const driver = {
    _id: "41224d776a326fb40f000001",
    email: 'driver@gmail.com',
    password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
    firstName: 'Driver ',
    lastName: 'driver',
    phoneNumber: '+48500000000',
    status: 'active'
};

const client = {
    _id: "41224d776a326fb40f000002",
    email: 'client@gmail.com',
    password: '$2a$10$zrWwoawjUnStC0tj.9XXpu0RH6hmpUQ3IOofL62HqofUVrTfg2DRG',
    firstName: 'Client ',
    lastName: 'client',
    phoneNumber: '+48500000000',
    status: 'active'
};

const tempDriverModel = {
    _id: '41224d776a326fb40f000003',
    email: 'asdljflsafj@gmail.com',
    password: '12345678',
    firstName: 'Jack',
    lastName: 'John',
    phoneNumber: '589843563456',
    status: DriverStatus.ACTIVE
};

const tempClientModel = {
    _id: "41224d776a326fb40f000004",
    email: 'sdkhaskfhkdf@gmail.com',
    password: '12345678',
    firstName: 'John',
    lastName: 'Doe',
    phoneNumber: '78945698239',
    status: ClientStatus.ACTIVE
};

const tempVehicleModel1 = {
    _id: "41224d776a326fb40f000005",
    make: 'BMW',
    yearOfManufacture: '2010',
    photos: [],
    status: VehicleStatus.ACTIVE
};

const tempVehicleModel2 = {
    make: 'Mercedes',
    yearOfManufacture: '2010',
    photos: [],
    status: VehicleStatus.ACTIVE
};

const transportEvent = {
    title: 'Deliver basketball team',
	description: 'Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce a quam. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Vestibulum volutpat pretium libero. Cras dapibus. Aliquam eu nunc. Proin faucibus arcu quis ante. Vestibulum volutpat pretium libero. Aliquam eu nunc.',
	locationFromCoordinates: {
		lat: 52.203992,
        lng: 21.013844,
        description: "Sapaya. Restauracja azjatycka"
	},
	locationToCoordinates: {
		lat: 52.167373,
        lng: 20.967948,
        description: "Warsaw Chopin Airport"
	},
	dateStart: "2019-03-10T10:45:00.000Z",
	dateEnd: "2019-03-10T13:15:00.000Z",
	drivers: ["41224d776a326fb40f000001"],
	clients: ["41224d776a326fb40f000002"],
	vehicle: "41224d776a326fb40f000005",
	createdAt: "2019-03-06T119:23:00.000Z",
    modifiedAt: "2019-03-06T19:24:00.000Z",
    status: "paid, finished"
};

const transportEventDriverClient = JSON.parse(JSON.stringify(transportEvent));
transportEventDriverClient.driver = '41224d776a326fb40f000003';
transportEventDriverClient.client = '41224d776a326fb40f000004';

export const initData = () => {
    return mongoose.connection.db.dropDatabase().then(() => {
        return Promise.all([
            new Driver(driver).save(),
            new Client(client).save(),
            new Admin(admin).save(),
            new Driver(tempDriverModel).save(),
            new Client(tempClientModel).save(),
            new Vehicle(tempVehicleModel1).save(),
            new Vehicle(tempVehicleModel2).save(),
            new TransportEvent(transportEvent).save(),
            new TransportEvent(transportEventDriverClient).save(),
        ]).then(([driver, client, admin, tempDriver, tempClient, tempVehicle1, tempVehicle2, transportEvent, transportEventDriverClient]) => {
            const driverToken = jwt.sign({
                _id: driver._id,
                email: driver.email,
                role: 'driver'
            }, tokenSecret, { expiresIn: 86400 });
    
            const clientToken = jwt.sign({
                _id: client._id,
                email: client.email,
                role: 'client'
            }, tokenSecret, { expiresIn: 86400 });
    
            const adminToken = jwt.sign({
                _id: admin._id,
                email: admin.email,
                role: 'admin'
            }, tokenSecret, { expiresIn: 86400 });
    
            drivers.push(tempDriver);
    
            const tempDriverToken = jwt.sign({
                _id: tempDriver._id,
                email: tempDriver.email,
                role: 'driver'
            }, tokenSecret, { expiresIn: 86400 });
    
            clients.push(tempClient);
    
            const tempClientToken = jwt.sign({
                _id: tempClient._id,
                email: tempClient.email,
                role: 'client'
            }, tokenSecret, { expiresIn: 86400 });
    
            return {
                driver,
                client,
                driverToken,
                clientToken,
                adminToken,
                admin,
                tempDriverToken,
                tempClientToken,
                tempDriver,
                tempClient,
                tempVehicle1,
                tempVehicle2,
                transportEvent,
                transportEventDriverClient
            };
        });
    });
};