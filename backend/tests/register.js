import request from 'supertest';
import async from 'async';

import app from '../app';
import Invitation from '../models/Invitation';
import Driver from '../models/Driver';
import Client from '../models/Client';

let adminToken = null;
let invitationToken = null;

const admin = {
    email: 'admin@gmail.com',
    password: '123456'
};

/**
 * Driver registration
 */
const driverInvitation = {
    email: 'jjgluskin@gmail.com',
    userRole: 'driver'
};

const driver = {
    email: 'jjgluskin@gmail.com',
    password: '12345678',
    firstName: 'Jack',
    lastName: 'John',
    phoneNumber: '727835660'
};
describe('POST /driver/register/:token', function () {

    this.timeout(10000);

    it('login admin, invite driver, register new driver', function (done) {
        async.series([
            function(cb) { 
                request(app)
                    .post('/api/login')
                    .send(admin)
                    .expect(200)
                    .end((err, res) => {
                        if(err) throw err;
                        adminToken = res.body.token;
                        cb();
                    })
            },

            function(cb) { 
                request(app)
                    .post('/api/invitation')
                    .set('Authorization', 'Bearer ' + adminToken)
                    .send(driverInvitation)
                    .expect(200, cb)
            },

            function(cb) {
                Invitation.find({email: driverInvitation.email}).then(({token}) => {
                    invitationToken = token;
                }).finally(cb);
            },

            function(cb) { 
                request(app)
                    .post('/api/driver/register/' + invitationToken)
                    .send(driver)
                    .expect(200, cb)
            },

            function(cb) {
                Promise.all([
                    Invitation.findOneAndRemove({email: driverInvitation.email}),
                    Driver.findOneAndRemove({email: driver.email})
                ]).then().finally(cb);
            }
        ], (err, result) => {
            if(err) {
                console.log(err, result.body);
            }
            done();
        });
    });
});

/**
 * Client registration
 */
const clientInvitation = {
    email: 'jjgluskin@gmail.com',
    userRole: 'client'
};

const client = {
    email: 'jjgluskin@gmail.com',
    password: '12345678',
    firstName: 'Jack',
    lastName: 'John',
    phoneNumber: '727835660'
};
describe('POST /client/register/:token', function () {

    this.timeout(10000);

    it('login admin, invite client, register new client', function (done) {
        async.series([
            function(cb) { 
                request(app)
                    .post('/api/login')
                    .send(admin)
                    .expect(200)
                    .end((err, res) => {
                        if(err) throw err;
                        adminToken = res.body.token;
                        cb();
                    })
            },

            function(cb) { 
                request(app)
                    .post('/api/invitation')
                    .set('Authorization', 'Bearer ' + adminToken)
                    .send(clientInvitation)
                    .expect(200, cb)
            },

            function(cb) {
                Invitation.find({email: clientInvitation.email}).then(({token}) => {
                    invitationToken = token;
                }).finally(cb);
            },

            function(cb) { 
                request(app)
                    .post('/api/client/register/' + invitationToken)
                    .send(client)
                    .expect(200, cb)
            },

            function(cb) {
                Promise.all([
                    Invitation.findOneAndRemove({email: clientInvitation.email}),
                    Client.findOneAndRemove({email: client.email})
                ]).then().finally(cb);
            }
        ], (err, result) => {
            if(err) {
                console.log(err, result.body);
            }
            done();
        });
    });
});