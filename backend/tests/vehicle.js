import request from 'supertest';

import app from '../app';
import Vehicle from '../models/Vehicle';
import validateDocument from '../utils/validateDocument';
import { initData } from './data';


describe('GET api/vehicle/', function () {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, driverToken, clientToken} = data);
            done();
        })
    });

    it('get all vehicles by driver', function (done) {
        request(app)
            .get('/api/vehicle')
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('get all vehicles by client', function (done) {
        request(app)
            .get('/api/vehicle')
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('get all vehicles by admin', function (done) {
        request(app)
            .get('/api/vehicle')
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Vehicles is not an array'));
                }
                for(const vehicle of res.body) {
                    const vehicleObj = new Vehicle(vehicle);
                    const errors = validateDocument(vehicleObj);
                    if(errors.length) {
                        return done(new Error(errors));
                    }
                }
                done();
            })
    });
});

describe('GET api/vehicle/{id}', function () {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let tempVehicle1 = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, tempVehicle1, driverToken, clientToken} = data);
            done();
        })
    });

    it('get vehicle by driver', function (done) {
        request(app)
            .get('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('get vehicle by client', function (done) {
        request(app)
            .get('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('get vehicle by admin', function (done) {
        request(app)
            .get('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(JSON.stringify(tempVehicle1) !== JSON.stringify(res.body)) {
                    return done(new Error('Returned object is not the proper'));
                }
                done();
            })
    });
});

describe('PUT api/vehicle/{id}', function() {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let tempVehicle1 = null;

    const propToTest = 'yearOfManufacture';

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, tempVehicle1, driverToken, clientToken} = data);
            done();
        })
    });

    it('edit vehicle by driver', function (done) {
        request(app)
            .put('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .send({[propToTest]: tempVehicle1[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit vehicle by client', function (done) {
        request(app)
            .put('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .send({[propToTest]: tempVehicle1[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit vehicle by admin', function(done) {
        request(app)
            .put('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .send({[propToTest]: tempVehicle1[propToTest]+'Testing'})
            .expect(200)
            .end(err => {
                if(err) throw err;
                Vehicle.findById(tempVehicle1._id).then(changedVehicle => {
                    for(const prop in tempVehicle1.toObject()) {
                        if(prop !== propToTest && JSON.stringify(changedVehicle[prop]) !== JSON.stringify(tempVehicle1[prop])) {
                            return done(new Error(`property ${prop} has changed unintentionally: from ${tempVehicle1[prop]} to ${changedVehicle[prop]}`));
                        } else if(prop === propToTest && changedVehicle[prop] === tempVehicle1[prop]) {
                            return done(new Error(`property ${prop} hasn't changed`));
                        } else if(prop === propToTest && changedVehicle[prop] !== tempVehicle1[prop]+'Testing') {
                            return done(new Error(`property ${prop} has change to improper value: from ${tempVehicle1[prop]} to ${changedVehicle[prop]}`));
                        }
                    }
                    done();
                }).catch(err => {
                    console.log(err);
                    done(err);
                })
            });
    });
});

describe('DELETE api/vehicle/{id}', function() {
    let adminToken = null;
    let driverToken = null;
    let clientToken = null;
    let tempVehicle1 = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, tempVehicle1, driverToken, clientToken} = data);
            done();
        })
    });

    it('delete vehicle by driver', function (done) {
        request(app)
            .delete('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + driverToken)
            .expect(401, done);
    });

    it('delete vehicle by client', function (done) {
        request(app)
            .delete('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('delete vehicle by admin', function(done) {
        request(app)
            .delete('/api/vehicle/'+tempVehicle1._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200, done);
    });
});