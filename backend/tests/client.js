import request from 'supertest';

import app from '../app';
import Client from '../models/Client';
import validateDocument from '../utils/validateDocument';
import { initData } from './data';


describe('GET api/client/', function () {
    let adminToken = null;
    let clientToken = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, clientToken} = data);
            done();
        })
    });

    it('get all client by client', function (done) {
        request(app)
            .get('/api/client')
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('get all client by admin', function (done) {
        request(app)
            .get('/api/client')
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(res.body.constructor !== Array) {
                    return done(new Error('Clients is not an array'));
                }
                for(const client of res.body) {
                    const clientObj = new Client(client);
                    const errors = validateDocument(clientObj);
                    if(errors.length) {
                        return done(new Error(errors));
                    }
                }
                done();
            })
    });
});

describe('GET api/client/{id}', function () {
    let adminToken = null;
    let clientToken = null;
    let tempClientToken = null;
    let client = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, clientToken, tempClientToken, client} = data);
            done();
        })
    });

    it('get client by another client', function (done) {
        request(app)
            .get('/api/client/'+client._id)
            .set('Authorization', 'Bearer ' + tempClientToken)
            .expect(401, done)
    });

    it('get client by the same client', function (done) {
        request(app)
            .get('/api/client/'+client._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(JSON.stringify(client) !== JSON.stringify(res.body)) {
                    return done(new Error('Returned object is not the proper'));
                }
                done();
            })
    });

    it('get client by admin', function (done) {
        request(app)
            .get('/api/client/'+client._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .expect(200)
            .end((err, res) => {
                if(err) throw err;
                if(JSON.stringify(client) !== JSON.stringify(res.body)) {
                    return done(new Error('Returned object is not the proper'));
                }
                done();
            })
    });
});

describe('PUT api/client/{id}', function() {
    let adminToken = null;
    let clientToken = null;
    let tempClientToken = null;
    let client = null;
    let tempClient = null;

    const propToTest = 'lastName';

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({adminToken, clientToken, tempClientToken, client, tempClient} = data);
            done();
        })
    });

    it('edit client by admin', function(done) {
        request(app)
            .put('/api/client/'+client._id)
            .set('Authorization', 'Bearer ' + adminToken)
            .send({[propToTest]: client[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit client by another client', function(done) {
        request(app)
            .put('/api/client/'+tempClient._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .send({[propToTest]: client[propToTest]+'Testing'})
            .expect(401, done);
    });

    it('edit client by the same client', function(done) {
        request(app)
            .put('/api/client/'+tempClient._id)
            .set('Authorization', 'Bearer ' + tempClientToken)
            .send({[propToTest]: tempClient[propToTest]+'Testing'})
            .expect(200)
            .end(err => {
                if(err) throw err;
                Client.findById(tempClient._id).then(changedClient => {
                    for(const prop in tempClient.toObject()) {
                        if(prop !== propToTest && JSON.stringify(changedClient[prop]) !== JSON.stringify(tempClient[prop])) {
                            return done(new Error(`property ${prop} has changed unintentionally`));
                        } else if(prop === propToTest && changedClient[prop] === tempClient[prop]) {
                            return done(new Error(`property ${prop} hasn't changed`));
                        } else if(prop === propToTest && changedClient[prop] !== tempClient[prop]+'Testing') {
                            return done(new Error(`property ${prop} has change to improper value: from ${tempClient[prop]} to ${changedClient[prop]}`));
                        }
                    }
                    done();
                });
            });
    });
});

describe('DELETE api/client/{id}', function() {
    let clientToken = null;
    let tempClientToken = null;
    let tempClient = null;

    this.timeout(10000);

    before(done => {
        initData().then(data => {
            ({clientToken, tempClientToken, tempClient} = data);
            done();
        })
    });

    it('delete client by another client', function(done) {
        request(app)
            .delete('/api/client/'+tempClient._id)
            .set('Authorization', 'Bearer ' + clientToken)
            .expect(401, done);
    });

    it('delete client by the same client', function(done) {
        request(app)
            .delete('/api/client/'+tempClient._id)
            .set('Authorization', 'Bearer ' + tempClientToken)
            .expect(200, done);
    });
});