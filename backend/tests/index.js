const mongoose = require('mongoose');
require("babel-core/register")({
    presets: [ "es2015", "stage-0" ]
});
require("babel-polyfill");

before(function(done) {
    mongoose.Promise = global.Promise;
    mongoose.connect("mongodb://localhost:27017/transporter_test", { useNewUrlParser: true })
        .then(() => {
            console.log("Connected to database!");
            mongoose.connection.db.dropDatabase().finally(done);
        })
        .catch(() => {
            console.log("Connection failed!");
        });
});

require('./login.js');
require('./register.js');
require('./driver.js');
require('./client.js');
require('./vehicle.js');
require('./transport-event.js');