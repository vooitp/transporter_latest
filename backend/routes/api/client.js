import bcrypt from 'bcrypt-nodejs';

import to from '../../utils/to';
import Client from '../../models/Client';
import TransportEvent from '../../models/TransportEvent';
import Invitation from '../../models/Invitation';
import validateDocument from '../../utils/validateDocument';
import { ClientStatus } from '../../utils/status';
import { UserRoles } from '../../utils/UserRoles';
import { getTransportEventsParams } from '../../utils/transportEvent';

/**
 * @swagger
 *
 * /api/client/register/{token}:
 *   post:
 *     tags:
 *       - client
 *     security: []
 *     description: Register user as client
 *     parameters:
 *       - name: token
 *         in: path
 *         required: true
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - email
 *             - password
 *             - firstName
 *             - lastName
 *             - phoneNumber
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             phoneNumber:
 *               type: string
 *           example: {
 *             "email": "email@gmail.com",
 *             "password": "12345678",
 *             "firstName": "John",
 *             "lastName": "Doe",
 *             "phoneNumber": "47593755"
 *           }
 *     responses:
 *       200:
 *         description: New client has been successfully registered
 *       500:
 *          description: Form or invitation expired issues
 */
export const registerClient = async (req, res) => {
    const data = req.body;
    const token = req.params.token;

    const [, invitation] = await to(Invitation.findOne({email: data.email, token}));
    const expirationDate = new Date(invitation.expirationDate);
    const expired = (expirationDate.getTime() < new Date().getTime());

    const client = new Client(data);
    client.status = ClientStatus.ACTIVE;

    const errors = validateDocument(client);

    if(!invitation) {
        errors.push({prop: 'invitation', message: 'No invitation was found with such email and token'});
    } else if(expired) {
        errors.push({prop: 'invitation', message: 'Invitation has expired'});
    }

    if(errors.length) {
        return res.status(500).send(errors);
    }

    client.password = bcrypt.hashSync(client.password);

    await Invitation.findByIdAndRemove(invitation._id);
    await client.save();
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/client/{id}:
 *   put:
 *     tags:
 *       - client
 *     summary: CLIENT
 *     description: Edit client data
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - firstName
 *             - lastName
 *             - phoneNumber
 *           properties:
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             phoneNumber:
 *               type: string
 *           example: {
 *             "firstName": "New name",
 *             "lastName": "New surname",
 *             "phoneNumber": "111111111"
 *           }
 *     responses:
 *       200:
 *         description: Client data has been successfully updated
 *       401:
 *         description: Current user is not authorized to edit this client
 *       404:
 *         description: Driver with such ID was not found
 *       500:
 *          description: Form or invitation expired issues
 */
export const editClient = async (req, res) => {
    const data = req.body;
    const id = req.params.id;

    const [, client] = await to(Client.findById(id));

    if(!client) {
        return res.status(404).send('no client found with such id');
    }

    if(req.user.role === UserRoles.CLIENT && id !== req.user._id) {
        return res.status(401).send();
    }

    if(typeof data.email !== 'undefined' || typeof data.password !== 'undefined') {
        return res.status(500).send('email and password can not be changed from this endpoint');
    }

    const updatedClient = new Client({
        ...client,
        ...data
    });

    const errors = validateDocument(updatedClient);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    await Client.replaceOne({_id: id}, updatedClient);
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/client:
 *   get:
 *     tags:
 *       - client
 *     summary: ADMIN
 *     description: Returns the list of all client
 *     responses:
 *       200:
 *         description: Array of client
 *       500:
 *          description: Database error
 */
export const getClients = async (req, res) => {
    const [err, clients] = await to(Client.find({}));

    if(clients) {
        res.status(200).send(clients);
    } else {
        res.status(500).send(err);
    }
};

/**
 * @swagger
 *
 * /api/client/{id}:
 *   get:
 *     tags:
 *       - client
 *     summary: ADMIN, CLIENT
 *     description: Returns data about particular client
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *       - name: includeTransportEvents
 *         in: query
 *         description: Boolean value which defines whether transport events should be attached to the response
 *       - name: transportEventsFromDate
 *         in: query
 *         description: Defines start date of transport events range in format "2019-03-10T10:45:00.000Z"
 *       - name: transportEventsToDate
 *         in: query
 *         description: Defines end date of transport events range in format "2019-03-11T10:45:00.000Z"
 *     responses:
 *       200:
 *         description: Object of client data
 *       401:
 *          description: Logged in client ID is not equal to ID in the URL
 *       404:
 *          description: Client with such ID was not found
 */
export const getClient = async (req, res) => {
    const id = req.params.id;

    if(req.user.role === UserRoles.CLIENT && req.user._id !== id) {
        return res.status(401).send();
    }

    const [, client] = await to(Client.findById(id).select('-password'));

    if(!client) {
        res.status(404).send('no client was found');
    }
    
    if(req.query.includeTransportEvents === 'true') {
        const fromDate = req.query.transportEventsFromDate;
        const toDate = req.query.transportEventsToDate;
        if(fromDate && isNaN(Date.parse(fromDate)) || (toDate && isNaN(Date.parse(toDate)))) {
            return res.status(500).send('Query parameter "transportEventsFromDate" and "transportEventsToDate" must of Date type if provided');
        }

        const {find, select, populate} = getTransportEventsParams(req.user.role, req.user._id, fromDate, toDate);
        const [err, transportEvents] = await to(TransportEvent.find(find).select(select).populate(populate));

        if(transportEvents) {
            client.transportEvents = transportEvents;
        } else {
            res.status(500).send(err);
        }
    }

    res.status(200).send(client);
};

/**
 * @swagger
 *
 * /api/client/{id}:
 *   delete:
 *     tags:
 *       - client
 *     summary: ADMIN, CLIENT
 *     description: Delete particular client
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Client was successfully deleted
 *       401:
 *         description: Logged in client ID is not equal to ID in the URL
 *       500:
 *          description: Database error
 */
export const deleteClient = async (req, res) => {
    const id = req.params.id;

    if(req.user.role === UserRoles.CLIENT && req.user._id !== id) {
        return res.status(401).send();
    }
        
    const [err] = await to(Client.findByIdAndRemove(id));

    // TODO should delete associated events

    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
}