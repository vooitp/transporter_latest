import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt-nodejs';

import to from '../../utils/to';
import Admin from '../../models/Admin';
import Client from '../../models/Client';
import Driver from '../../models/Driver';
import { tokenSecret } from '../../utils/tokenSecret';

export const tokenValidation = roles => {
    return (req, res, next) => {
        let token = req.get('Authorization');

        if(!token) {
            return res.status(401).send();
        }
    
        jwt.verify(token.substr(7), tokenSecret, (err, decodedToken) => {
            if(err && err.message === 'jwt expired') {
                return res.status(401).send('token_expired');
            }

            if(err) {
                return res.status(401).send('invalid_token');
            }

            if(roles.indexOf(decodedToken.role) === -1) {
                return res.status(401).send('invalid_role');
            }
    
            req.user = decodedToken;
    
            next();
        });
    };
}

/**
 * @swagger
 *
 * /api/login:
 *   post:
 *     tags:
 *       - auth
 *     security: []
 *     description: Login the user
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - email
 *             - password
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *           example: {
 *             "email": "admin@gmail.com",
 *             "password": "123456"
 *           }
 *     responses:
 *       200:
 *         description: Returns object of token and role
 *       500:
 *          description: Email or password is missing
 *       404:
 *          description: User not found
 */
export const login = async (req, res) => {
    const data = req.body;

    if(!data.email || !data.password) {
        return res.status(500).send('email and/or password are/is not provided');
    }

    let user = null;
    let userRole = null;

    [, user] = await to(Admin.findOne({email: data.email}));
    userRole = 'admin';

    if(!user) {
        [, user] = await to(Driver.findOne({email: data.email}));
        userRole = 'driver';
    }

    if(!user) {
        [, user] = await to(Client.findOne({email: data.email}));
        userRole = 'client';
    }

    if(!user || !bcrypt.compareSync(data.password, user.password)) {
        return res.status(404).send('login and/or password are/is incorrect');
    }

    const token = jwt.sign({
        _id: user._id,
        email: user.email,
        role: userRole
    }, tokenSecret, { expiresIn: 86400 });

    res.status(200).send({token, role: userRole});
}