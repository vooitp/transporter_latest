import uuidv4 from 'uuid/v4';

import Invitation from '../../models/Invitation';
import Driver from '../../models/Driver';
import Client from '../../models/Client';
import { mailer } from '../../utils/mailer';
import validateDocument from '../../utils/validateDocument';
import to from '../../utils/to';
import { UserRoles } from '../../utils/UserRoles';

/**
 * @swagger
 *
 * /api/invitation:
 *   post:
 *     tags:
 *       - invitation
 *     summary: ADMIN
 *     description: Create invitation for new user
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - email
 *             - userRole
 *           properties:
 *             email:
 *               type: string
 *             userRole:
 *               type: string
 *           example: {
 *             "email": "email@gmail.com",
 *             "userRole": "driver"
 *           }
 *     responses:
 *       200:
 *         description: Invitation has been successfully created and sent to defined email
 *       500:
 *          description: Form or invitation issues
 */
export const addInvitation = async (req, res) => {
    const data = req.body;

    const [, invitationExists] = await to(Invitation.findOne({email: data.email}));

    const invitation = new Invitation(data);
    // invitation validity
    const in24hours = 60000 * 96 + new Date().getTime();
    // const in24hours = 60000 * 24 + new Date().getTime();

    invitation.expirationDate = new Date(in24hours).toISOString();
    invitation.token = uuidv4();

    const errors = validateDocument(invitation);

    const [, driverExists] = await to(Driver.findOne({email: invitation.email}));
    const [, clientExists] = await to(Client.findOne({email: invitation.email}));

    if(invitationExists) {
        errors.push({prop: 'email', message: 'Invitation to this email has already been sent'});
    } else if(!invitation.userRole || !UserRoles[invitation.userRole.toUpperCase()]) {
        errors.push({prop: 'userRole', message: 'Invalid user role'});
    } else if(driverExists) {
        errors.push({prop: 'email', message: 'Driver with such email already exists'});
    } else if(clientExists) {
        errors.push({prop: 'email', message: 'Client with such email already exists'});
    }

    if(errors.length) {
        return res.status(500).send(errors);
    }

    await invitation.save();
    if(process.env.NODE_ENV !== 'test') {
        await mailer(
            invitation.email, 
            `    <div>
                    <div style="background: grey; height: 80px;    display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;-webkit-box-pack: center;-ms-flex-pack: center; justify-content: center;"> 
                        <img style="text-align: center; margin: 0 auto;" src="http://murmuring-meadow-21822.herokuapp.com/frontend/assets/images/komfort_bus_small_logo_white.png"/>
                    </div>
                    <div>Dear user,</div>
                    <div>You have been invited to become ${invitation.userRole} on <a href="https://transporter.pl">transporter.pl</a>.</div>
                    <div>Use <a href="https://murmuring-meadow-21822.herokuapp.com/${invitation.userRole}/register/${invitation.token}/${invitation.email}">this link</a> to register.</div>
                </div>
            `,
            'Invitation to "Transporter"',
        );
    }
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/invitation:
 *   get:
 *     tags:
 *       - invitation
 *     summary: ADMIN
 *     description: Returns the list of all invitations
 *     responses:
 *       200:
 *         description: Array of invitations
 *       500:
 *          description: Database error
 */
export const getInvitations = async (req, res) => {
    const [err, invitations] = await to(Invitation.find({}));

    if(invitations) {
        res.status(200).send(invitations);
    } else {
        res.status(500).send(err);
    }
};

/**
 * @swagger
 *
 * /api/invitation/{id}:
 *   get:
 *     tags:
 *       - invitation
 *     summary: ADMIN
 *     description: Returns data about particular invitation
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Object of invitation data
 *       404:
 *          description: Invitation with such ID was not found
 */
export const getInvitation = async (req, res) => {
    const id = req.params.id;
    const [, invitation] = await to(Invitation.findById(id));

    if(invitation) {
        res.status(200).send(invitation);
    } else {
        res.status(404).send('no invitation was found');
    }
};

/**
 * @swagger
 *
 * /api/invitation/check/{token}/{email}:
 *   get:
 *     tags:
 *       - invitation
 *     security: []
 *     description: Check whether invitation with particular token and email exists
 *     parameters:
 *       - name: token
 *         required: true
 *         in: path
 *       - name: email
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Object of invitation data
 *       404:
 *          description: Invitation with such ID was not found
 */
export const checkInvitation = async (req, res) => {
    const token = req.params.token;
    const email = req.params.email;

    const [, invitationExists] = await to(Invitation.findOne({token, email}));

    if(invitationExists) {
        res.status(200).send();
    } else {
        res.status(404).send();
    }
}

/**
 * @swagger
 *
 * /api/invitation/{id}:
 *   delete:
 *     tags:
 *       - invitation
 *     summary: ADMIN
 *     description: Delete particular invitation
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Invitation was successfully deleted
 *       500:
 *          description: Database error
 */
export const deleteInvitation = async (req, res) => {
    const id = req.params.id;
        
    const [err] = await to(Invitation.findByIdAndRemove(id));

    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
}