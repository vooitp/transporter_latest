import bcrypt from 'bcrypt-nodejs';

import to from '../../utils/to';
import Driver from '../../models/Driver';
import TransportEvent from '../../models/TransportEvent';
import Invitation from '../../models/Invitation';
import validateDocument from '../../utils/validateDocument';
import { DriverStatus } from '../../utils/status';
import { UserRoles } from '../../utils/UserRoles';
import { getTransportEventsParams } from '../../utils/transportEvent';

/**
 * @swagger
 *
 * /api/driver/register/{token}:
 *   post:
 *     tags:
 *       - driver
 *     security: []
 *     description: Register user as driver
 *     parameters:
 *       - name: token
 *         in: path
 *         required: true
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - email
 *             - password
 *             - firstName
 *             - lastName
 *             - phoneNumber
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             phoneNumber:
 *               type: string
 *           example: {
 *             "email": "email@gmail.com",
 *             "password": "12345678",
 *             "firstName": "John",
 *             "lastName": "Doe",
 *             "phoneNumber": "47593755"
 *           }
 *     responses:
 *       200:
 *         description: New driver has been successfully registered
 *       500:
 *          description: Form or invitation expired issues
 */
export const registerDriver = async (req, res) => {
    const data = req.body;
    const token = req.params.token;

    const [, invitation] = await to(Invitation.findOne({email: data.email, token}));
    const expirationDate = new Date(invitation.expirationDate);
    const expired = (expirationDate.getTime() < new Date().getTime());

    const driver = new Driver(data);
    driver.status = DriverStatus.ACTIVE;

    const errors = validateDocument(driver);

    if(!invitation) {
        errors.push({prop: 'invitation', message: 'No invitation was found with such email and token'});
    } else if(expired) {
        errors.push({prop: 'invitation', message: 'Invitation has expired'});
    }

    if(errors.length) {
        return res.status(500).send(errors);
    }

    driver.password = bcrypt.hashSync(driver.password);

    await Invitation.findByIdAndRemove(invitation._id);
    await driver.save();
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/driver/{id}:
 *   put:
 *     tags:
 *       - driver
 *     summary: DRIVER
 *     description: Edit driver data
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - firstName
 *             - lastName
 *             - phoneNumber
 *           properties:
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             phoneNumber:
 *               type: string
 *           example: {
 *             "firstName": "New name",
 *             "lastName": "New surname",
 *             "phoneNumber": "111111111"
 *           }
 *     responses:
 *       200:
 *         description: Driver data has been successfully updated
 *       401:
 *         description: Current user is not authorized to edit this driver
 *       404:
 *         description: Driver with such ID was not found
 *       500:
 *          description: Form or invitation expired issues
 */
export const editDriver = async (req, res) => {
    const data = req.body;
    const id = req.params.id;

    const [, driver] = await to(Driver.findById(id));

    if(!driver) {
        return res.status(404).send('no driver found with such id');
    }

    if(req.user.role === UserRoles.DRIVER && id !== req.user._id) {
        return res.status(401).send();
    }

    if(typeof data.email !== 'undefined' || typeof data.password !== 'undefined') {
        return res.status(500).send('email and password can not be changed from this endpoint');
    }

    const updatedDriver = new Driver({
        ...driver,
        ...data
    });

    const errors = validateDocument(updatedDriver);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    await Driver.replaceOne({_id: id}, updatedDriver);
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/driver/:
 *   get:
 *     tags:
 *       - driver
 *     summary: ADMIN
 *     description: Returns the list of all driver
 *     responses:
 *       200:
 *         description: Array of drivers
 *       500:
 *          description: Unexpected database error
 */
export const getDrivers = async (req, res) => {
    const [err, drivers] = await to(Driver.find({}));

    if(drivers) {
        res.status(200).send(drivers);
    } else {
        res.status(500).send(err);
    }
};

/**
 * @swagger
 *
 * /api/driver/{id}:
 *   get:
 *     tags:
 *       - driver
 *     summary: ADMIN, DRIVER
 *     description: Returns data about particular driver
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *       - name: includeTransportEvents
 *         in: query
 *         description: Boolean value which defines whether transport events should be attached to the response
 *       - name: transportEventsFromDate
 *         in: query
 *         description: Defines start date of transport events range in format "2019-03-10T10:45:00.000Z"
 *       - name: transportEventsToDate
 *         in: query
 *         description: Defines end date of transport events range in format "2019-03-11T10:45:00.000Z"
 *     responses:
 *       200:
 *         description: Object of driver data
 *       401:
 *          description: Logged in driver ID is not equal to ID in the URL
 *       404:
 *          description: Driver with such ID was not found
 */
export const getDriver = async (req, res) => {
    const id = req.params.id;
    const [, driver] = await to(Driver.findById(id).select('-password'));

    if(req.user.role === UserRoles.DRIVER && req.user._id !== id) {
        return res.status(401).send();
    }

    if(!driver) {
        res.status(404).send('no driver was found');
    }
    
    if(req.query.includeTransportEvents === 'true') {
        const fromDate = req.query.transportEventsFromDate;
        const toDate = req.query.transportEventsToDate;
        if(fromDate && isNaN(Date.parse(fromDate)) || (toDate && isNaN(Date.parse(toDate)))) {
            return res.status(500).send('Query parameter "transportEventsFromDate" and "transportEventsToDate" must of Date type if provided');
        }

        const {find, select, populate} = getTransportEventsParams(UserRoles.DRIVER, id, fromDate, toDate);
        const [err, transportEvents] = await to(TransportEvent.find(find).select(select).populate(populate));

        if(transportEvents) {
            driver.transportEvents = transportEvents;
        } else {
            res.status(500).send(err);
        }
    }

    res.status(200).send(driver);
};

/**
 * @swagger
 *
 * /api/driver/{id}:
 *   delete:
 *     tags:
 *       - driver
 *     summary: ADMIN, DRIVER
 *     description: Delete particular driver
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Driver was successfully deleted
 *       401:
 *         description: Logged in driver ID is not equal to ID in the URL
 *       500:
 *          description: Database error
 */
export const deleteDriver = async (req, res) => {
    const id = req.params.id;

    if(req.user.role === UserRoles.DRIVER && req.user._id !== id) {
        return res.status(401).send();
    }

    // TODO should delete associated events
        
    const [err] = await to(Driver.findByIdAndRemove(id));

    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
}