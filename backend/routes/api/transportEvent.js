import to from '../../utils/to';
import TransportEvent from '../../models/TransportEvent';
import validateDocument from '../../utils/validateDocument';
import { getTransportEventsParams } from '../../utils/transportEvent';

/**
 * @swagger
 *
 * /api/transport-event:
 *   post:
 *     tags:
 *       - transport event
 *     summary: ADMIN
 *     description: Create new transport event
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - locationFromCoordinates
 *             - locationToCoordinates
 *             - date
 *             - drivers
 *             - clients
 *             - vehicle
 *           properties:
 *             locationFromCoordinates:
 *               type: object
 *               properties:
 *                  lat:
 *                      type: number
 *                  lng:
 *                      type: number
 *                  description:
 *                      type: string
 *             locationToCoordinates:
 *               type: object
 *               properties:
 *                  lat:
 *                      type: number
 *                  lng:
 *                      type: number
 *                  description:
 *                      type: string
 *             date:
 *               type: string
 *             drivers:
 *               type: array
 *               description: drivers' IDs
 *             clients:
 *               type: array
 *               description: clients' IDs
 *             vehicle:
 *               type: string
 *               description: vehicle ID
 *           example: {
 *             "locationFromCoordinates": {
 *                 "lat": 14,
 *                 "lng": 34,
 *                 "description": "Near Blue City"
 *             },
 *             "locationToCoordinates": {
 *                 "lat": 78,
 *                 "lng": 35,
 *                 "description": "Wola Park"
 *             },
 *             "date": "2019-02-24T11:08:52.304Z",
 *             "driver": ["5c6f27a147edcda98738bc90"],
 *             "client": ["5c6f27a147edcda98738bc91"],
 *             "vehicle": "5c6f27a147edcda98738bc92",
 *             "status": "paid, finished"
 *           }
 *     responses:
 *       200:
 *         description: New client has been successfully registered
 *       500:
 *          description: Form issues
 */
export const addTransportEvent = async (req, res) => {
    const data = req.body;

    const transportEvent = new TransportEvent(data);

    const errors = validateDocument(transportEvent);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    transportEvent.createdAt = new Date().toISOString();

    await transportEvent.save();
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/transport-event/edit/{id}:
 *   put:
 *     tags:
 *       - transport event
 *     summary: ADMIN
 *     description: Edit transport event data
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - locationFromCoordinates
 *             - locationToCoordinates
 *             - date
 *             - drivers
 *             - clients
 *             - vehicle
 *           properties:
 *             locationFromCoordinates:
 *               type: object
 *               properties:
 *                  lat:
 *                      type: number
 *                  lng:
 *                      type: number
 *                  description:
 *                      type: string
 *             locationToCoordinates:
 *               type: object
 *               properties:
 *                  lat:
 *                      type: number
 *                  lng:
 *                      type: number
 *                  description:
 *                      type: string
 *             date:
 *               type: string
 *             drivers:
 *               type: array
 *               description: drivers' IDs
 *             clients:
 *               type: array
 *               description: clients' IDs
 *             vehicle:
 *               type: string
 *               description: vehicle ID
 *           example: {
 *             "locationFromCoordinates": {
 *                 "lat": 14,
 *                 "lng": 34,
 *                 "description": "Near Blue City"
 *             },
 *             "locationToCoordinates": {
 *                 "lat": 78,
 *                 "lng": 35,
 *                 "description": "Wola Park"
 *             },
 *             "date": "2019-02-24T11:08:52.304Z",
 *             "driver": "5c6f27a147edcda98738bc90",
 *             "client": "5c6f27a147edcda98738bc91",
 *             "vehicle": "5c6f27a147edcda98738bc92"
 *           }
 *     responses:
 *       200:
 *         description: Client data has been successfully updated
 *       404:
 *         description: Driver with such ID was not found
 *       500:
 *          description: Form or invitation expired issues
 */
export const editTransportEvent = async (req, res) => {
    const data = req.body;
    const id = req.params.id;

    const [, transportEvent] = await to(TransportEvent.findById(id));

    if(!transportEvent) {
        return res.status(404).send('no event found with such id');
    }

    const updatedTransportEvent = new TransportEvent({
        ...transportEvent,
        ...data
    });

    const errors = validateDocument(updatedTransportEvent);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    updatedTransportEvent.modifiedAt = new Date().toISOString();

    await TransportEvent.replaceOne({_id: id}, updatedTransportEvent);
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/transport-event:
 *   get:
 *     tags:
 *       - transport event
 *     summary: ADMIN, CLIENT, DRIVER
 *     description: Returns the list of all transport events available for this user. If only one parameter from "fromDate" and "toDate" is provided, then "limit" parameter is required
 *     parameters:
 *       - name: fromDate
 *         in: query
 *         description: Start date of transport events range in format "2019-03-10T10:45:00.000Z"
 *       - name: toDate
 *         in: query
 *         description: End date of transport events range in format "2019-03-11T10:45:00.000Z"
 *       - name: limit
 *         in: query
 *         description: Number of results to be returned
 *     responses:
 *       200:
 *         description: Array of transport events
 *       500:
 *          description: Database error
 */
export const getTransportEvents = async (req, res) => {
    const fromDate = req.query.fromDate;
    const toDate = req.query.toDate;
    const limit = req.query.limit;

    const fromDateValid = (fromDate && !isNaN(Date.parse(fromDate)));
    const toDateValid = (toDate && !isNaN(Date.parse(toDate)));
    const limitValid = (limit && !isNaN(parseInt(limit)));

    if(!fromDateValid && !toDateValid) {
        return res.status(500).send('One of parameters "fromDate" and "toDate" is required and must of Date type');
    } else if((fromDateValid ^ toDateValid) && !limitValid) {
        return res.status(500).send('"limit" parameter is required and must be a number if only one of parameters "fromDate" and "toDate" is provided');
    }

    const {find, select, populate} = getTransportEventsParams(req.user.role, req.user._id, fromDate, toDate);
    const query = TransportEvent.find(find).select(select).populate(populate);
    if(fromDateValid ^ toDateValid) {
        query.limit(parseInt(limit));
    }
    const [, transportEvents] = await to(query);

    if(transportEvents) {
        res.status(200).send(transportEvents);
    } else {
        res.status(500).send(err);
    }
};

/**
 * @swagger
 *
 * /api/transport-event/{id}:
 *   get:
 *     tags:
 *       - transport event
 *     summary: ADMIN, CLIENT, DRIVER
 *     description: Returns data about particular transport event
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Object of client data
 *       404:
 *          description: Transport event with such ID was not found
 */
export const getTransportEvent = async (req, res) => {
    const {find, select, populate} = getTransportEventsParams(req.user.role, req.user._id);
    find._id = req.params.id;

    const [, transportEvent] = await to(TransportEvent.findOne(find).select(select).populate(populate));

    if(transportEvent) {
        res.status(200).send(transportEvent);
    } else {
        res.status(404).send('no event was found');
    }
};

/**
 * @swagger
 *
 * /api/transport-event/{id}:
 *   delete:
 *     tags:
 *       - transport event
 *     summary: ADMIN
 *     description: Delete particular transport event
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Transport event was successfully deleted
 *       500:
 *          description: Database error
 */
export const deleteTransportEvent = async (req, res) => {
    const id = req.params.id;
        
    const [err] = await to(TransportEvent.findByIdAndRemove(id));

    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
}