import to from '../../utils/to';
import Vehicle from '../../models/Vehicle';
import validateDocument from '../../utils/validateDocument';
import { VehicleStatus } from '../../utils/status';
import imageParser from '../../utils/imageParser';

const VehiclesFolder = '/media/vehicles/';

/**
 * @swagger
 *
 * /api/vehicle:
 *   post:
 *     tags:
 *       - vehicle
 *     summary: ADMIN
 *     description: Add new vehicle
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - make
 *             - yearOfManufacture
 *             - photos
 *           properties:
 *             make:
 *               type: string
 *             yearOfManufacture:
 *               type: string
 *             photos:
 *               type: array
 *               items:
 *                 - type: string
 *           example: {
 *             "make": "BMW",
 *             "yearOfManufacture": "2010",
 *             "photos": [
 *                  "base64..."
 *              ]
 *           }
 *     responses:
 *       200:
 *         description: New vehicle has been successfully registered
 *       500:
 *          description: Form issues
 */
export const addVehicle = async (req, res) => {
    const data = req.body;

    const vehicle = new Vehicle(data);
    vehicle.status = VehicleStatus.ACTIVE;

    const errors = validateDocument(vehicle);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    vehicle.photos = imageParser(VehiclesFolder, vehicle.photos, []);

    await vehicle.save();
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/vehicle/{id}:
 *   put:
 *     tags:
 *       - vehicle
 *     summary: ADMIN
 *     description: Edit vehicle data
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *             - make
 *             - yearOfManufacture
 *             - photos
 *           properties:
 *             make:
 *               type: string
 *             yearOfManufacture:
 *               type: string
 *             photos:
 *               type: array
 *               items:
 *                 - type: string
 *           example: {
 *             "make": "BMW",
 *             "yearOfManufacture": "2010",
 *             "photos": [
 *                  "base64...",
 *                  "/media/img/image1.png"
 *              ]
 *           }
 *     responses:
 *       200:
 *         description: Vehicle data has been successfully updated
 *       404:
 *         description: Vehicle with such ID was not found
 *       500:
 *          description: Form issues
 */
export const editVehicle = async (req, res) => {
    const data = req.body;
    const id = req.params.id;

    const [, vehicle] = await to(Vehicle.findById(id));

    if(!vehicle) {
        return res.status(404).send('no vehicle found with such id');
    }

    data.photos = imageParser(VehiclesFolder, data.photos, vehicle.photos);
    data.status = VehicleStatus.ACTIVE;

    const updatedVehicle = new Vehicle({
        ...vehicle,
        ...data
    });

    const errors = validateDocument(updatedVehicle);

    if(errors.length) {
        return res.status(500).send(errors);
    }

    await Vehicle.replaceOne({_id: id}, updatedVehicle);
    res.status(200).send();
};

/**
 * @swagger
 *
 * /api/vehicle:
 *   get:
 *     tags:
 *       - vehicle
 *     summary: ADMIN
 *     description: Returns the list of all vehicles
 *     responses:
 *       200:
 *         description: Array of vehicles
 *       500:
 *          description: Database error
 */
export const getVehicles = async (req, res) => {
    const [err, vehicles] = await to(Vehicle.find({}));

    if(vehicles) {
        res.status(200).send(vehicles);
    } else {
        res.status(500).send(err);
    }
};

/**
 * @swagger
 *
 * /api/vehicle/{id}:
 *   get:
 *     tags:
 *       - vehicle
 *     summary: ADMIN
 *     description: Returns data about particular vehicle
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Object of vehicle data
 *       404:
 *          description: Vehicle with such ID was not found
 */
export const getVehicle = async (req, res) => {
    const id = req.params.id;
    const [, vehicle] = await to(Vehicle.findById(id));

    if(vehicle) {
        res.status(200).send(vehicle);
    } else {
        res.status(404).send('no vehicle was found');
    }
};

/**
 * @swagger
 *
 * /api/vehicle/{id}:
 *   delete:
 *     tags:
 *       - vehicle
 *     summary: ADMIN
 *     description: Delete particular vehicle
 *     parameters:
 *       - name: id
 *         required: true
 *         in: path
 *     responses:
 *       200:
 *         description: Vehicle was successfully deleted
 *       500:
 *          description: Database error
 */
export const deleteVehicle = async (req, res) => {
    const id = req.params.id;

    const [, vehicle] = await to(Vehicle.findById(id));

    imageParser(VehiclesFolder, [], vehicle.photos);   
        
    const [err] = await to(Vehicle.findByIdAndRemove(id));

    // TODO should delete associated events

    if(err) {
        res.status(500).send(err);
    } else {
        res.status(200).send();
    }
}