import { tokenValidation, login } from './api/auth';

import {addTransportEvent, editTransportEvent, getTransportEvent, getTransportEvents, deleteTransportEvent} from './api/transportEvent';
import {addVehicle, editVehicle, getVehicle, getVehicles, deleteVehicle} from './api/vehicle';
import {registerClient, editClient, getClient, getClients, deleteClient} from './api/client';
import {registerDriver, editDriver, getDriver, getDrivers, deleteDriver} from './api/driver';
import { getInvitation, getInvitations, addInvitation, deleteInvitation, checkInvitation } from './api/invitation';
import { UserRoles } from '../utils/UserRoles';

const protect = roles => {
    return [tokenValidation(roles)];
};

/**
 * API routes
 */

export default app => {
    app.post('/api/login', login);

    app.get('/api/invitation/', protect([UserRoles.ADMIN]), getInvitations);
    app.get('/api/invitation/:id', protect([UserRoles.ADMIN]), getInvitation);
    app.post('/api/invitation', protect([UserRoles.ADMIN]), addInvitation);
    app.delete('/api/invitation/:id', protect([UserRoles.ADMIN]), deleteInvitation);
    app.get('/api/invitation/check/:token/:email', checkInvitation);

    app.get('/api/transport-event/', protect([UserRoles.ADMIN, UserRoles.DRIVER, UserRoles.CLIENT]), getTransportEvents);
    app.get('/api/transport-event/:id', protect([UserRoles.ADMIN, UserRoles.DRIVER, UserRoles.CLIENT]), getTransportEvent);
    app.post('/api/transport-event', protect([UserRoles.ADMIN]), addTransportEvent);
    app.put('/api/transport-event/:id', protect([UserRoles.ADMIN]), editTransportEvent);
    app.delete('/api/transport-event/:id', protect([UserRoles.ADMIN]), deleteTransportEvent);

    app.get('/api/vehicle/', protect([UserRoles.ADMIN]), getVehicles);
    app.get('/api/vehicle/:id', protect([UserRoles.ADMIN]), getVehicle);
    app.post('/api/vehicle', protect([UserRoles.ADMIN]), addVehicle);
    app.put('/api/vehicle/:id', protect([UserRoles.ADMIN]), editVehicle);
    app.delete('/api/vehicle/:id', protect([UserRoles.ADMIN]), deleteVehicle);

    app.get('/api/client/', protect([UserRoles.ADMIN]), getClients);
    app.get('/api/client/:id', protect([UserRoles.ADMIN, UserRoles.CLIENT]), getClient);
    app.post('/api/client/register/:token', registerClient);
    app.put('/api/client/:id', protect([UserRoles.ADMIN, UserRoles.CLIENT]), editClient);
    app.delete('/api/client/:id', protect([UserRoles.ADMIN, UserRoles.CLIENT]), deleteClient);

    app.get('/api/driver/', protect([UserRoles.ADMIN]), getDrivers);
    app.get('/api/driver/:id', protect([UserRoles.ADMIN, UserRoles.DRIVER]), getDriver);
    app.post('/api/driver/register/:token', registerDriver);
    app.put('/api/driver/:id', protect([UserRoles.ADMIN, UserRoles.DRIVER]), editDriver);
    app.delete('/api/driver/:id', protect([UserRoles.ADMIN, UserRoles.DRIVER]), deleteDriver);
}